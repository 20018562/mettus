package it.mat20018562.meetus

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.View
import android.widget.Button
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import it.mat20018562.meetus.model.serviceViewModel
import it.mat20018562.meetus.model.userViewModel
import it.mat20018562.meetus.service.MyServiceRecyclerAdapter


class ServicesActivity : AppCompatActivity(), MyServiceRecyclerAdapter.ItemClickListener  {

    private lateinit var currentUser: userViewModel

    private val servicesList = ArrayList<serviceViewModel>()
    private var adapter: MyServiceRecyclerAdapter? = null

    //private val user = FirebaseAuth.getInstance().currentUser
    private val db = FirebaseFirestore.getInstance()

    private var serviceTmp : serviceViewModel = serviceViewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_services)

        currentUser = intent.getSerializableExtra("currentUser") as userViewModel

        //logDebug("Load activity services from serviceS")
        /*val btnCreateService = findViewById<Button>(R.id.btn_createService)
        btnCreateService.setOnClickListener{
            val intent = Intent(this,ServiceActivity::class.java)
            intent.putExtra("id","new")
            startActivity(intent)
        }*/

        db.collection("services")
            .whereEqualTo("userId", currentUser.id)
            .get()
            .addOnSuccessListener { documents ->
                //logDebug("Success get service")
                for (document in documents) {
                    serviceTmp = document.toObject(serviceViewModel::class.java)
                    serviceTmp.id = document.id
                    //logDebug("ServiceTmp ID:" + serviceTmp.id)
                    servicesList.add(serviceTmp)
                }

                val recyclerView = findViewById<RecyclerView>(R.id.rv_service)
                recyclerView.layoutManager = LinearLayoutManager(this)
                //logDebug("Step1")
                adapter = MyServiceRecyclerAdapter(this, servicesList)
                adapter!!.hideCompanyName = 1
                adapter!!.setClickListener(this)
                recyclerView.adapter = adapter
                //logDebug("Step2")
            }
            .addOnFailureListener { exception ->
                logError("Error getting documents: ", exception)
            }
    }

    override fun onItemClick(view: View?, position: Int) {
        val intent = Intent(this,ServiceActivity::class.java)
        intent.putExtra("id",adapter!!.getItem(position).id)
        intent.putExtra("currentUser",currentUser)
        startActivity(intent)
    }
}