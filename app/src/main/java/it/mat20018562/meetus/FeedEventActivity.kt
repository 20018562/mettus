package it.mat20018562.meetus

import android.app.Notification
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FieldValue
import com.google.firebase.firestore.FirebaseFirestore
import it.mat20018562.meetus.model.feedModel
import it.mat20018562.meetus.model.feedViewModel
import it.mat20018562.meetus.model.serviceViewModel
import it.mat20018562.meetus.model.userViewModel
import it.mat20018562.meetus.service.MyFeedRecyclerAdapter
import it.mat20018562.meetus.service.MyServiceRecyclerAdapter

class FeedEventActivity : AppCompatActivity(), MyFeedRecyclerAdapter.ItemClickListener {

    private val feedList = ArrayList<feedViewModel>()
    private var adapter: MyFeedRecyclerAdapter? = null

    private lateinit var currentUser: userViewModel
    private val db = FirebaseFirestore.getInstance()

    private var feedTmp : feedViewModel = feedViewModel()

    private var eventId : String? = null

    private lateinit var et_comment: EditText
    private lateinit var ll_oriPost: LinearLayout

    private lateinit var et_creatorPostOri: TextView
    private lateinit var et_textPostOri: TextView


    private var idReplyFeed: String? = null
    private var notificationManager: NotificationManager? = null

    //post padre
    private var emailPostParent: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_feed_event)

        currentUser = intent.getSerializableExtra("currentUser") as userViewModel

        notificationManager =getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        val btnSendComment = findViewById<Button>(R.id.btn_sendComment)
        btnSendComment.setOnClickListener{
            SendComment()
        }
        et_comment = findViewById(R.id.et_comment)
        et_creatorPostOri =findViewById(R.id.et_creatorPostOri)
        et_textPostOri =findViewById(R.id.et_textPostOri)
        ll_oriPost = findViewById(R.id.ll_oriPost)

        eventId= intent.getStringExtra("eventId").toString()
        idReplyFeed= intent.getStringExtra("idReplyFeed").toString()
        //idReplyFeed

        loadReply()
        /*if(idReplyFeed!="0"){

            db.collection("feedEvent").document(idReplyFeed!!)
                .get()
                .addOnSuccessListener { document ->
                    //logDebug("Success get feed")
                        feedTmp = document.toObject(feedViewModel::class.java)!!
                        et_creatorPostOri.text = feedTmp.emailIdCreator
                        et_textPostOri.text = feedTmp.text
                        emailPostParent = feedTmp.emailIdCreator
                }
                .addOnFailureListener { exception ->
                    logError("Error getting documents: ", exception)
                }
                loadReply()
        }else {
           ll_oriPost.visibility = View.GONE
            db.collection("feedEvent")
                .whereEqualTo("eventId", eventId)
                .whereEqualTo("idReplyFeed", "0")
                .get()
                .addOnSuccessListener { documents ->
                    //logDebug("Success get service")
                    for (document in documents) {
                        feedTmp = document.toObject(feedViewModel::class.java)
                        feedTmp.id = document.id
                        //logDebug("ServiceTmp ID:" + serviceTmp.id)
                        feedList.add(feedTmp)
                    }

                    val recyclerView = findViewById<RecyclerView>(R.id.rv_feed)
                    recyclerView.layoutManager = LinearLayoutManager(this)
                    //logDebug("Step1")
                    adapter = MyFeedRecyclerAdapter(this, feedList)
                    adapter!!.setClickListener(this)
                    recyclerView.adapter = adapter
                    //logDebug("Step2")
                }
                .addOnFailureListener { exception ->
                    logError("Error getting documents: ", exception)
                }
        }*/
    }

    private fun loadReply() {
        feedList.clear()
        if(idReplyFeed!="0"){

            db.collection("feedEvent").document(idReplyFeed!!)
                .get()
                .addOnSuccessListener { document ->
                    //logDebug("Success get feed")
                    feedTmp = document.toObject(feedViewModel::class.java)!!
                    et_creatorPostOri.text = feedTmp.emailIdCreator
                    et_textPostOri.text = feedTmp.text
                    emailPostParent = feedTmp.emailIdCreator
                }
                .addOnFailureListener { exception ->
                    logError("Error getting documents: ", exception)
                }
            db.collection("feedEvent")
                .whereEqualTo("idReplyFeed", idReplyFeed)
                .get()
                .addOnSuccessListener { documents ->
                    //logDebug("Success get service")
                    for (document in documents) {
                        feedTmp = document.toObject(feedViewModel::class.java)
                        feedTmp.id = document.id
                        //logDebug("ServiceTmp ID:" + serviceTmp.id)
                        feedList.add(feedTmp)
                    }

                    val recyclerView = findViewById<RecyclerView>(R.id.rv_feed)
                    recyclerView.layoutManager = LinearLayoutManager(this)
                    //logDebug("Step1")
                    adapter = MyFeedRecyclerAdapter(this, feedList)
                    adapter!!.setClickListener(this)
                    recyclerView.adapter = adapter
                    //logDebug("Step2")
                }
                .addOnFailureListener { exception ->
                    logError("Error getting documents: ", exception)
                }
        }else {
            ll_oriPost.visibility = View.GONE
            db.collection("feedEvent")
                .whereEqualTo("eventId", eventId)
                .whereEqualTo("idReplyFeed", "0")
                .get()
                .addOnSuccessListener { documents ->
                    //logDebug("Success get service")
                    for (document in documents) {
                        feedTmp = document.toObject(feedViewModel::class.java)
                        feedTmp.id = document.id
                        //logDebug("ServiceTmp ID:" + serviceTmp.id)
                        feedList.add(feedTmp)
                    }

                    val recyclerView = findViewById<RecyclerView>(R.id.rv_feed)
                    recyclerView.layoutManager = LinearLayoutManager(this)
                    //logDebug("Step1")
                    adapter = MyFeedRecyclerAdapter(this, feedList)
                    adapter!!.setClickListener(this)
                    recyclerView.adapter = adapter
                    //logDebug("Step2")
                }
                .addOnFailureListener { exception ->
                    logError("Error getting documents: ", exception)
                }
        }
    }

    override fun onItemClick(view: View?, position: Int) {
        val intent = Intent(this,FeedEventActivity::class.java)
        intent.putExtra("eventId",eventId)
        intent.putExtra("idReplyFeed",adapter!!.getItem(position).id)
        intent.putExtra("currentUser",currentUser)
        startActivity(intent)
    }

    fun SendComment(){
        var feed = feedModel()
        if(et_comment.text.toString()=="") {
            Toast.makeText(this, R.string.needComment, Toast.LENGTH_LONG).show()
        }else {
            feed.text = et_comment.text.toString()
            feed.eventId = eventId
            feed.emailIdCreator = currentUser.email
            feed.idReplyFeed = idReplyFeed
            db.collection("feedEvent").document().set(feed)
                .addOnSuccessListener {
                    //Toast.makeText(this, R.string.saveSuccess, Toast.LENGTH_LONG).show()
                    sendNotification()
                    /*val intent = Intent(this, EventsActivity::class.java)
                    intent.putExtra("eventId", eventId)
                    intent.putExtra("currentUser", currentUser)
                    startActivity(intent)*/
                    if(idReplyFeed!="0")
                        db.collection("feedEvent").document(idReplyFeed!!).update("nreply", FieldValue.increment(1))
                    loadReply()
                }
                .addOnFailureListener { exception ->
                    logError(exception.message.toString())
                }
        }
    }

    fun sendNotification() {

        val notificationID = 1
        val resultIntent = Intent(this, LoginActivity::class.java)
        val pendingIntent = PendingIntent.getActivity(
            this,
            0,
            resultIntent,
            PendingIntent.FLAG_UPDATE_CURRENT
        )

        var channelID = "it.meetus.feed"

        var notification = Notification.Builder(this@FeedEventActivity,
            channelID)
            .setContentTitle("Nuovo post all'evento")
            .setContentText("")
            .setSmallIcon(android.R.drawable.sym_def_app_icon)
            .setChannelId(channelID)
            .setContentIntent(pendingIntent)
            .build()

        notificationManager?.notify(notificationID, notification)


        //NOTIFICA ALLA CANALRE UTENTE
        channelID = "it.meetus.feed.$emailPostParent"
        logDebug("EMAIL Parent $emailPostParent")
        notification = Notification.Builder(this@FeedEventActivity,
            channelID)
            .setContentTitle("Nuovo post all'evento")
            .setContentText("")
            .setSmallIcon(android.R.drawable.sym_def_app_icon)
            .setChannelId(channelID)
            .setContentIntent(pendingIntent)
            .build()

        notificationManager?.notify(notificationID, notification)
    }

}