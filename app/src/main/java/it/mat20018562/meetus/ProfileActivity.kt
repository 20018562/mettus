package it.mat20018562.meetus

import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.location.Geocoder
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.maps.model.LatLng
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import it.mat20018562.meetus.model.markerModel
import it.mat20018562.meetus.model.userModel
import it.mat20018562.meetus.model.userViewModel

class ProfileActivity : AppCompatActivity() {

    //private var _userService = UserService();

    //private val user = FirebaseAuth.getInstance().currentUser
    private val db = FirebaseFirestore.getInstance()

    private var notificationManager: NotificationManager? = null

    private lateinit var currentUser: userViewModel

    private lateinit var tvemail: TextView
    private lateinit var tvname: TextView
    private lateinit var tvsurname: TextView
    private lateinit var tv_nameCompany: TextView
    private lateinit var tvaddressCompany: TextView
    private lateinit var tvpiva: TextView
    private lateinit var tviban: TextView
    private lateinit var tvcf: TextView
    private lateinit var tvresidence: TextView
    private lateinit var llCategory: LinearLayout

    private lateinit var sp_categoryUser: Spinner
    private lateinit var sp_notifyFeed: Spinner

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)

        currentUser = intent.getSerializableExtra("currentUser") as userViewModel

        val btnsave = findViewById<Button>(R.id.btn_save)
        btnsave.setOnClickListener{
            saveData()
        }

        notificationManager =
            getSystemService(
                Context.NOTIFICATION_SERVICE) as NotificationManager

        tvemail = findViewById(R.id.tv_email)
        tvname = findViewById(R.id.tv_name)
        tvsurname = findViewById(R.id.tv_surname)
        tv_nameCompany= findViewById(R.id.tv_nameCompany) //fornitore
        tvaddressCompany = findViewById(R.id.tv_addressCompany) //fornitore
        tvpiva = findViewById(R.id.tv_piva)         //fornitore
        tviban = findViewById(R.id.tv_iban)           //fornitore

        tvcf = findViewById(R.id.tv_cf)          //organizzatore
        tvresidence = findViewById(R.id.tv_residence)      //organizzatore

        sp_categoryUser = findViewById(R.id.sp_categoryUser)
        sp_notifyFeed = findViewById(R.id.sp_notifyFeed)

        llCategory = findViewById(R.id.llCategory)

        ArrayAdapter.createFromResource(
            this,
            R.array.categoryUserList,
            android.R.layout.simple_spinner_item
        ).also { adapter ->
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            sp_categoryUser.adapter = adapter
        }

        ArrayAdapter.createFromResource(
            this,
            R.array.notifyFeedList,
            android.R.layout.simple_spinner_item
        ).also { adapter ->
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            sp_notifyFeed.adapter = adapter
        }

        db.collection("users")
            .document(currentUser.id!!)
            .get()
            .addOnSuccessListener { document ->
                if (document != null) {
                    currentUser = document.toObject(userViewModel::class.java)!!
                    currentUser.id = document.id

                    tvemail.text = currentUser.email
                    tvname.text = currentUser.name
                    tvsurname.text = currentUser.surname
                    tv_nameCompany.text = currentUser.nameCompany
                    tvaddressCompany.text = currentUser.addressCompany
                    tvpiva.text = currentUser.piva
                    tviban.text = currentUser.iban
                    tvcf.text = currentUser.cf
                    tvresidence.text = currentUser.residence

                    for (i in 0 until sp_categoryUser.getCount()) {
                        if (sp_categoryUser.getItemAtPosition(i).toString().equals(currentUser.categoryUser)) {
                            sp_categoryUser.setSelection(i);
                        }
                    }

                    for (i in 0 until sp_notifyFeed.getCount()) {
                        if (sp_notifyFeed.getItemAtPosition(i).toString().equals(currentUser.notifyFeed)) {
                            sp_notifyFeed.setSelection(i);
                        }
                    }

                    if(currentUser.role=="organizzatore"){
                        tv_nameCompany.visibility = View.GONE
                        tvaddressCompany.visibility = View.GONE
                        tvpiva.visibility = View.GONE
                        tviban.visibility = View.GONE
                        llCategory.visibility = View.GONE
                    }else if(currentUser.role=="fornitore"){
                        tvcf.visibility = View.GONE
                        tvresidence.visibility = View.GONE
                        llCategory.visibility = View.GONE
                    }else if(currentUser.role=="invitato"){
                        tv_nameCompany.visibility = View.GONE
                        tvaddressCompany.visibility = View.GONE
                        tvpiva.visibility = View.GONE
                        tviban.visibility = View.GONE

                    }
                }
            }
            .addOnFailureListener { exception ->
                logError(exception.message.toString())
            }
    }

    private fun saveData() {
        currentUser.email = tvemail.text.toString()
        currentUser.name = tvname.text.toString()
        currentUser.surname = tvsurname.text.toString()
        currentUser.nameCompany = tv_nameCompany.text.toString()
        currentUser.addressCompany = tvaddressCompany.text.toString()
        currentUser.piva = tvpiva.text.toString()
        currentUser.iban = tviban.text.toString()
        currentUser.cf = tvcf.text.toString()
        currentUser.residence = tvresidence.text.toString()
        currentUser.editProfile = 0
        if(currentUser.role=="invitato")
            currentUser.categoryUser = sp_categoryUser.selectedItem.toString()
        currentUser.notifyFeed = sp_notifyFeed.selectedItem.toString()
        if(currentUser.role=="fornitore"){
            var coordinate = getLocationByAddress(tvaddressCompany.text.toString())
            currentUser.latitude = coordinate?.latitude
            currentUser.longitude = coordinate?.longitude
            //logDebug("latlng: " + currentUser.coordinates?.latitude.toString() + "," + currentUser.coordinates?.latitude.toString())
        }
        //Canale personale per le notifiche
        createNotificationChannel(
            "it.meetus.personal." + currentUser.email,
            "Notifiche personali meetus",
            "Notifiche a :" + currentUser.email)

        if(currentUser.notifyFeed=="tutte"){
            createNotificationChannel(
                "it.meetus.feed",
                "Tutti i Feed meetus",
                "Tutti i post di meetus")
            notificationManager?.deleteNotificationChannel("it.meetus.feed" + currentUser.email);
        }else if(currentUser.notifyFeed=="propri post"){
            createNotificationChannel(
                "it.meetus.feed." + currentUser.email,
                "Feed personali meetus",
                "I miei post di meetus")
            notificationManager?.deleteNotificationChannel("it.meetus.feed" + currentUser.email);
        }else{
            //nessuna notifica
            notificationManager?.deleteNotificationChannel("it.meetus.feed");
            notificationManager?.deleteNotificationChannel("it.meetus.feed." + currentUser.email);
            notificationManager?.deleteNotificationChannel("it.meetus.personal." + currentUser.email);
        }

        db.collection("users").document(currentUser.id!!).set(currentUser)
            .addOnSuccessListener {
                Toast.makeText(this,R.string.saveSuccess, Toast.LENGTH_LONG).show()
            }
            .addOnFailureListener { exception ->
                logError(exception.message.toString())
            }
    }

    private fun createNotificationChannel(id: String, name: String,
                                          description: String) {

        val importance = NotificationManager.IMPORTANCE_LOW
        val channel = NotificationChannel(id, name, importance)

        channel.description = description

        notificationManager?.createNotificationChannel(channel)
    }

    fun getLocationByAddress(strAddress: String?): LatLng? {
        val coder = Geocoder(this)
        try {
            val address = coder.getFromLocationName(strAddress, 5) ?: return null
            val location = address.first()
            /*
            val builder = LatLng.newBuilder()
            builder.latitude = location.latitude
            builder.longitude = location.longitude
            var latLng = builder.build()
            */
            return LatLng(location.latitude, location.longitude)
        } catch (e: Exception) {
            logError(e.message.toString())
        }
        return null
    }
}