package it.mat20018562.meetus

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Geocoder
import android.location.Location
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.core.app.ActivityCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.model.LatLng
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.maps.android.SphericalUtil
import it.mat20018562.meetus.model.*
import it.mat20018562.meetus.service.MyServiceRecyclerAdapter
import java.time.format.DateTimeFormatter
import kotlin.math.roundToInt


class EventActivity : AppCompatActivity(), MyServiceRecyclerAdapter.ItemClickListener  {

    //private val user = FirebaseAuth.getInstance().currentUser
    private val db = FirebaseFirestore.getInstance()

    private lateinit var fusedLocationClient: FusedLocationProviderClient

    private var event: eventModel = eventModel()

    private lateinit var sp_typeEvent: Spinner
    private lateinit var sp_typeServiceEvent: Spinner
    private lateinit var tv_date: TextView
    private lateinit var tv_addressEvent: TextView
    private lateinit var tv_budget: TextView
    private lateinit var rv_service: RecyclerView
    private lateinit var btnViewMap: Button
    private lateinit var llListEvent: LinearLayout
    private lateinit var lltypeServiceEvent: LinearLayout
    private lateinit var tv_distance: TextView

    private lateinit var currentUser: userViewModel

    private var eventId: String = ""

    private var serviceTmp : serviceViewModel = serviceViewModel()
    private val servicesList = ArrayList<serviceViewModel>()
    private var adapter: MyServiceRecyclerAdapter? = null



    private var latitude: Double = 0.0;
    private var longitude: Double = 0.0;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_event)

        tv_distance = findViewById(R.id.tv_distance)

        eventId= intent.getStringExtra("id").toString()
        currentUser = intent.getSerializableExtra("currentUser") as userViewModel
        //logDebug("EventId passed: " + eventId)

        sp_typeEvent = findViewById(R.id.sp_typeEvent)
        sp_typeServiceEvent = findViewById(R.id.sp_typeServiceEvent)
        tv_date = findViewById(R.id.tv_date)
        tv_addressEvent = findViewById(R.id.tv_addressEvent)
        tv_budget = findViewById(R.id.tv_budget)
        rv_service = findViewById(R.id.rv_service)
        llListEvent = findViewById(R.id.llListEvent)
        lltypeServiceEvent= findViewById(R.id.lltypeServiceEvent)

        val btnSaveEvent = findViewById<Button>(R.id.btn_saveEvent)
        val btnSaveServiceEvent = findViewById<Button>(R.id.btn_saveServiceEvent)
        val btnSendInvite = findViewById<Button>(R.id.btn_sendInvite)
        val btnFeed = findViewById<Button>(R.id.btn_viewFeed)
        val btnPhoto = findViewById<Button>(R.id.btn_viewPhoto)
        val btnViewMap = findViewById<Button>(R.id.btnViewMap)

        /*FOR HIDE OR DISPLAY*/
        if(eventId=="new"){
            sp_typeServiceEvent.visibility = View.GONE
            btnSaveServiceEvent.visibility = View.GONE
            btnSendInvite.visibility = View.GONE
            btnFeed.visibility = View.GONE
            btnPhoto.visibility = View.GONE
            rv_service.visibility = View.GONE
            llListEvent.visibility = View.GONE
            lltypeServiceEvent.visibility = View.GONE
        }else{
            loadEvent()
            loadRecivleViewServiceEvent()
        }
        /*
        db.collection("users")
            .document(user?.uid.toString())
            .get()
            .addOnSuccessListener { document ->
                if (document != null) {
                    currentUser = document.toObject(userModel::class.java)!!*/

                    if(currentUser.role=="invitato"){
                        btnSaveServiceEvent.visibility = View.GONE
                        btnSendInvite.visibility = View.GONE
                    }
                /*}
            }
            .addOnFailureListener { exception ->
                logError(exception.message.toString())
            }*/


        ArrayAdapter.createFromResource(
            this,
            R.array.typeEventList,
            android.R.layout.simple_spinner_item
        ).also { adapter ->
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            sp_typeEvent.adapter = adapter
        }

        ArrayAdapter.createFromResource(
            this,
            R.array.categoryList,
            android.R.layout.simple_spinner_item
        ).also { adapter ->
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            sp_typeServiceEvent.adapter = adapter
        }


        btnSaveEvent.setOnClickListener{
            saveEvent()
        }

        btnSaveServiceEvent.setOnClickListener{
            saveServiceEvent()
        }

        btnSendInvite.setOnClickListener{
            sendInvite()
        }
        btnViewMap.setOnClickListener{
            viewMap()
        }


        btnFeed.setOnClickListener{
            val intent = Intent(this,FeedEventActivity::class.java)
            intent.putExtra("eventId",eventId)
            intent.putExtra("idReplyFeed","0")
            intent.putExtra("currentUser",currentUser)
            startActivity(intent)
        }
        btnPhoto.setOnClickListener{
            val intent = Intent(this,PhotoEventActivity::class.java)
            intent.putExtra("eventId",eventId)
            intent.putExtra("currentUser",currentUser)
            //logDebug("Start Photo")
            startActivity(intent)
        }


    }

    private fun loadDistance() {
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            ActivityCompat.requestPermissions(this,
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), 1);
            return
        }
        fusedLocationClient.lastLocation
            .addOnSuccessListener { location : Location? ->
                // Got last known location. In some rare situations this can be null.
                latitude = location!!.latitude
                longitude = location!!.longitude
                //LoadEvents(currentUser);
                tv_distance.text = "Dis.: " + SphericalUtil.computeDistanceBetween(LatLng(event.latitude!!, event.longitude!!), LatLng(latitude, longitude)).roundToInt();
            }
    }

    private fun viewMap() {
        if (event.latitude == null || event.longitude == null){
            Toast.makeText(this, R.string.needAddress, Toast.LENGTH_LONG).show()
        }else{
            val intent = Intent(this,MapsActivity::class.java)
            val markerList = ArrayList<markerModel>()
            markerList.add(markerModel(event.latitude!!, event.longitude!!, event.typeEvent!!))
            intent.putExtra("markerList",markerList)
            intent.putExtra("currentUser",currentUser)
            startActivity(intent)
        }

    }

    private fun loadEvent(){
        //logDebug("Select event " + eventId)
        db.collection("events")
            .document(eventId)
            .get()
            .addOnSuccessListener { document ->
                //logDebug("Service selection success" )
                if (document != null) {
                    /*
                    logDebug("Event : " + document["typeEvent"])
                    logDebug("Event : " + document["date"])
                    logDebug("Event : " + document["addressEvent"])*/

                    event = document.toObject(eventModel::class.java)!!

                    tv_date.text = event.date
                    tv_addressEvent.text = event.addressEvent
                    tv_budget.text = event.budget.toString()

                    for (i in 0 until sp_typeEvent.getCount()) {
                        if (sp_typeEvent.getItemAtPosition(i).toString().equals(event.typeEvent)) {
                            sp_typeEvent.setSelection(i);
                        }
                    }
                    if(event.latitude!= null)
                        loadDistance()
                }
            }
            .addOnFailureListener { exception ->
                logError(exception.message.toString())
            }
    }

    private fun loadRecivleViewServiceEvent(){
        servicesList.clear()
        db.collection("events")
            .document(eventId)
            .collection("serviceEvent")
            .get()
            .addOnSuccessListener { documents ->
                //logDebug("Success get service")
                for (document in documents) {
                    //serviceTmp.id = document.get("eventId").toString()
                    //serviceTmp.name = document.get("serviceType").toString()
                    //logDebug("Event ID:" + document.id + " Name service: " + serviceTmp.name)
                    var evServ = document.toObject(serviceEventViewModel::class.java)
                    var serv = serviceViewModel()
                    serv.name = evServ.serviceType
                    serv.id = evServ.eventId
                    servicesList.add(serv)
                }

                for(ser in servicesList){
                    logDebug("service added: " + ser.name)
                }

                val recyclerView = findViewById<RecyclerView>(R.id.rv_service)
                recyclerView.layoutManager = LinearLayoutManager(this)
                //logDebug("Step1")
                adapter = MyServiceRecyclerAdapter(this, servicesList)
                adapter!!.setClickListener(this)
                recyclerView.adapter = adapter
                //logDebug("Step2")
            }
            .addOnFailureListener { exception ->
                logError("Error getting documents: ", exception)
            }
    }

    private fun saveEvent() {

        event.typeEvent = sp_typeEvent.selectedItem.toString()
        event.date = tv_date.text.toString()
        event.addressEvent = tv_addressEvent.text.toString()
        var coordinate = getLocationByAddress( event.addressEvent)
        event.latitude = coordinate?.latitude
        event.longitude = coordinate?.longitude

        event.budget = tv_budget.text.toString().toDouble()
        event.userIdCreator = currentUser.id

        if(eventId == "new") {
            db.collection("events").document().set(event)
                .addOnSuccessListener {
                    //Toast.makeText(this, R.string.saveSuccess, Toast.LENGTH_LONG).show()
                    val intent = Intent(this,OrganizerActivity::class.java)
                    intent.putExtra("currentUser",currentUser)
                    startActivity(intent)
                }
                .addOnFailureListener { exception ->
                    Toast.makeText(this, R.string.saveFailed, Toast.LENGTH_LONG).show()
                    logError(exception.message.toString())
                }
        }else{
            db.collection("events").document(eventId).set(event)
                .addOnSuccessListener {

                    val intent = Intent(this,OrganizerActivity::class.java)
                    intent.putExtra("currentUser",currentUser)
                    startActivity(intent)
                }
                .addOnFailureListener { exception ->
                    Toast.makeText(this, R.string.saveFailed, Toast.LENGTH_LONG).show()
                    logError(exception.message.toString())
                }
        }
    }

    private fun saveServiceEvent(){
        var serviceEvent = eventServiceModel()
        serviceEvent.eventId = eventId
        serviceEvent.serviceType = sp_typeServiceEvent.selectedItem.toString()
        db.collection("events").document(eventId).collection("serviceEvent").document().set(serviceEvent)
            .addOnSuccessListener {
                loadRecivleViewServiceEvent()
                //Toast.makeText(this, R.string.saveSuccess, Toast.LENGTH_LONG).show()
                //val intent = Intent(this,EventActivity::class.java)
                //intent.putExtra("currentUser",currentUser)
                //startActivity(intent)
            }
            .addOnFailureListener { exception ->
                logError(exception.message.toString())
            }
    }

    fun sendInvite(){
        val intent = Intent(this,SendInviteActivity::class.java)
        intent.putExtra("eventId",eventId)
        intent.putExtra("currentUser",currentUser)
        startActivity(intent)
    }

    override fun onItemClick(view: View?, position: Int) {
        //if(currentUser!=null){
            if(currentUser.role == "organizzatore"){
                //val intent = Intent(this,SupplierListForServiceInEventActivity::class.java)
                val intent = Intent(this,SuppliersActivity::class.java)
                intent.putExtra("serviceType",adapter!!.getItem(position).name)
                intent.putExtra("eventId",eventId)
                intent.putExtra("currentUser",currentUser)
                startActivity(intent)
            }
            //logDebug("USER " + currentUser!!.email.toString() + " ServiceId = "+ adapter!!.getItem(position).id)
        /*}else{
            Toast.makeText(this, R.string.userNotLoad, Toast.LENGTH_LONG).show()
        }*/
    }


    fun getLocationByAddress(strAddress: String?): LatLng? {
        val coder = Geocoder(this)
        try {
            val address = coder.getFromLocationName(strAddress, 5) ?: return null
            val location = address.first()
              return LatLng(location.latitude, location.longitude)
        } catch (e: Exception) {
            logError(e.message.toString())
        }
        return null
    }
}