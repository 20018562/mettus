package it.mat20018562.meetus

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.ktx.Firebase
import it.mat20018562.meetus.model.meetingViewModel
import it.mat20018562.meetus.model.serviceViewModel
import it.mat20018562.meetus.model.userViewModel
import it.mat20018562.meetus.service.MyMeetingRecyclerAdapter
import it.mat20018562.meetus.service.MyServiceRecyclerAdapter

class SupplierActivity : AppCompatActivity(), MyMeetingRecyclerAdapter.ItemClickListener {

    private lateinit var currentUser: userViewModel

    private val db = FirebaseFirestore.getInstance()

    private var adapter: MyMeetingRecyclerAdapter? = null

    private val meetingList = ArrayList<meetingViewModel>()
    private var meeting: meetingViewModel = meetingViewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_supplier)

        currentUser = intent.getSerializableExtra("currentUser") as userViewModel

        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar);

        db.collection("meeting")
            .whereEqualTo("supplierUserId", currentUser.id)
            .get()
            .addOnSuccessListener { documents ->
                //logDebug("Success get meet")
                for (document in documents) {
                    meeting = document.toObject(meetingViewModel::class.java)
                    meeting.id = document.id
                    //logDebug("Meet ID:" + meeting.id)
                    meetingList.add(meeting)
                }

                val recyclerView = findViewById<RecyclerView>(R.id.rv_meeting)
                recyclerView.layoutManager = LinearLayoutManager(this)
                //logDebug("Step1")
                adapter = MyMeetingRecyclerAdapter(this, meetingList)
                adapter!!.setClickListener(this)
                recyclerView.adapter = adapter
                //logDebug("Step2")
            }
            .addOnFailureListener { exception ->
                logError("Error getting documents: ", exception)
            }

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.supplier_menu, menu);
        return true;
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId) {
            R.id.logout -> {
                Firebase.auth.signOut()
                startActivity(Intent(this,LoginActivity::class.java))
            }
            R.id.newService -> {
                val intent = Intent(this,ServiceActivity::class.java)
                intent.putExtra("id","new")
                intent.putExtra("currentUser",currentUser)
                startActivity(intent)
            }
            R.id.services -> {
                //logDebug("Load activity services from service")
                val intent = Intent(this,ServicesActivity::class.java)
                intent.putExtra("currentUser",currentUser)
                startActivity(intent)
            }
            R.id.profile ->{
                val intent = Intent(this,ProfileActivity::class.java)
                //logDebug("ID from supplier activity" + currentUser.id)
                intent.putExtra("currentUser",currentUser)
                startActivity(intent)
            }

        }
        return super.onOptionsItemSelected(item)
    }

    override fun onItemClick(view: View?, position: Int) {
        val intent = Intent(this,MeetingActivity::class.java)
        intent.putExtra("currentUser",currentUser)
        intent.putExtra("id",adapter!!.getItem(position).id)
        startActivity(intent)
    }
}