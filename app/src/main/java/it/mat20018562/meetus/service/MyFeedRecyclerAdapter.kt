package it.mat20018562.meetus.service

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import it.mat20018562.meetus.App
import it.mat20018562.meetus.R
import it.mat20018562.meetus.model.feedViewModel

class MyFeedRecyclerAdapter internal constructor(context: Context?, data: List<feedViewModel>)
    : RecyclerView.Adapter<MyFeedRecyclerAdapter.ViewHolder>() {

    private val mData: List<feedViewModel>
    private val mInflater: LayoutInflater
    private var mClickListener: ItemClickListener? = null

    interface ItemClickListener {
        fun onItemClick(view: View?, position: Int)
    }

    init {
        mData = data
        mInflater = LayoutInflater.from(context)
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = mInflater.inflate(R.layout.row_feed, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val itemValue = mData[position]
        //logDebug("Event from adapter: " + itemValue.name)
        holder.tv_text.text = itemValue.text
        holder.tv_creator.text = itemValue.emailIdCreator
        holder.tv_nReply.text =  App.context?.resources?.getString(R.string.nReply) + " " +  itemValue.nreply.toString()
    }

    override fun getItemCount(): Int {
        return mData.size
    }

    fun setClickListener(itemClickListener: ItemClickListener?) {
        mClickListener = itemClickListener
    }

    inner class ViewHolder internal constructor (itemView: View) :
        RecyclerView.ViewHolder(itemView), View.OnClickListener {

        var tv_text: TextView
        var tv_creator: TextView
        var tv_nReply: TextView

        override fun onClick(view: View?) {
            if (mClickListener != null) {
                mClickListener!!.onItemClick(view, adapterPosition)
            }
        }

        init {
            tv_text = itemView.findViewById(R.id.tv_text)
            tv_creator = itemView.findViewById(R.id.tv_creator)
            tv_nReply = itemView.findViewById(R.id.tv_nReply)
            itemView.setOnClickListener(this)
        }

    }
    fun getItem(id:Int): feedViewModel {
        return mData[id]
    }
}