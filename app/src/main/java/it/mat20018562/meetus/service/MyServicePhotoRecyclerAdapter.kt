package it.mat20018562.meetus.service

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import it.mat20018562.meetus.R
import it.mat20018562.meetus.model.serviceModel
import it.mat20018562.meetus.model.servicePhotoModel
import it.mat20018562.meetus.model.serviceViewModel
import android.graphics.BitmapFactory

import android.graphics.Bitmap
import android.util.Base64
import com.bumptech.glide.Glide
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.FirebaseStorage
import it.mat20018562.meetus.logDebug
import it.mat20018562.meetus.logError
import java.lang.Exception


class MyServicePhotoRecyclerAdapter internal constructor(context: Context?, data: List<servicePhotoModel>)
    : RecyclerView.Adapter<MyServicePhotoRecyclerAdapter.ViewHolder>() {

    private val mData: List<servicePhotoModel>
    private val mInflater: LayoutInflater
    private var mClickListener: ItemClickListener? = null

    interface ItemClickListener {
        fun onItemClick(view: View?, position: Int)
    }

    init {
        mData = data
        mInflater = LayoutInflater.from(context)
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = mInflater.inflate(R.layout.img_service, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        try {
            val itemValue = mData[position]
            //logDebug("INIT DOWN FROM Storage path: images/${itemValue.serviceId}/${itemValue.id}")
            FirebaseStorage.getInstance().reference.child("images/${itemValue.serviceId}/${itemValue.id}").downloadUrl.addOnSuccessListener { uri ->
                val imageURL = uri.toString()
                    //Glide.with(binding.root).load(imageURL).into(binding.cartImageItem)
                Glide.with(holder.itemView).load(imageURL).into(holder.iv_image)
                //logDebug("TEST IMG " + imageURL)
            }
            /*
            //holder.tv_nameService.text = itemValue.base64
            logDebug(itemValue.base64.toString())

            val decodedString: ByteArray = Base64.decode(itemValue.base64, Base64.DEFAULT)
            val decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.size)
            holder.iv_image.setImageBitmap(decodedByte)

            //val imageAsBytes: ByteArray = Base64.decode(itemValue.getBytes(), Base64.DEFAULT)
            //holder.iv_image.setImageBitmap(BitmapFactory.decodeByteArray(imageAsBytes, 0, imageAsBytes.size))

            logDebug("SetBitmap")*/
        }catch (e:Exception){
            logError(e.message.toString())
        }
    }

    override fun getItemCount(): Int {
        return mData.size
    }

    fun setClickListener(itemClickListener: ItemClickListener?) {
        mClickListener = itemClickListener
    }

    inner class ViewHolder internal constructor (itemView: View) :
        RecyclerView.ViewHolder(itemView), View.OnClickListener {

        var iv_image: ImageView

        override fun onClick(view: View?) {
            if (mClickListener != null) {
                mClickListener!!.onItemClick(view, adapterPosition)
            }
        }

        init {
            iv_image = itemView.findViewById(R.id.iv_image)
            itemView.setOnClickListener(this)
        }

    }

    fun getItem(id:Int): servicePhotoModel {
        return mData[id]
    }
}