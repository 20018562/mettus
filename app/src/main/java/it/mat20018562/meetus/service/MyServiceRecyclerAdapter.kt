package it.mat20018562.meetus.service

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import it.mat20018562.meetus.R
import it.mat20018562.meetus.logDebug
import it.mat20018562.meetus.model.serviceModel
import it.mat20018562.meetus.model.serviceViewModel

class MyServiceRecyclerAdapter internal constructor(context: Context?, data: List<serviceViewModel>)
    : RecyclerView.Adapter<MyServiceRecyclerAdapter.ViewHolder>() {

    private val mData: List<serviceViewModel>
    private val mInflater: LayoutInflater
    private var mClickListener: ItemClickListener? = null

    var hideCompanyName = 0

    interface ItemClickListener {
        fun onItemClick(view: View?, position: Int)
    }

    init {
        mData = data
        mInflater = LayoutInflater.from(context)
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = mInflater.inflate(R.layout.row_service, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val itemValue = mData[position]
        //logDebug("Event from adapter: " + itemValue.name)
        holder.tv_nameService.text = itemValue.name
        holder.tv_companyName.text = itemValue.companyName
        if(hideCompanyName==1)
            holder.tv_companyName.visibility = View.GONE
    }

    override fun getItemCount(): Int {
        return mData.size
    }

    fun setClickListener(itemClickListener: ItemClickListener?) {
        mClickListener = itemClickListener
    }

    inner class ViewHolder internal constructor (itemView: View) :
        RecyclerView.ViewHolder(itemView), View.OnClickListener {

        var tv_nameService: TextView
        var tv_companyName: TextView
        //var tv_idService: ImageView

        override fun onClick(view: View?) {
            if (mClickListener != null) {
                mClickListener!!.onItemClick(view, adapterPosition)
            }
        }

        init {
            tv_nameService = itemView.findViewById(R.id.tv_nameService)
            tv_companyName =itemView.findViewById(R.id.tv_companyName)
            //tv_idService = itemView.findViewById(R.id.tv_idService)
            itemView.setOnClickListener(this)
        }

    }
    fun getItem(id:Int): serviceViewModel {
        return mData[id]
    }
}