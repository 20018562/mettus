package it.mat20018562.meetus.service

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import it.mat20018562.meetus.R
import it.mat20018562.meetus.logDebug
import it.mat20018562.meetus.model.*

class MySuppliersRecyclerAdapter internal constructor(context: Context?, data: List<userViewModel>)
    : RecyclerView.Adapter<MySuppliersRecyclerAdapter.ViewHolder>() {

    private val mData: List<userViewModel>
    private val mInflater: LayoutInflater
    private var mClickListener: ItemClickListener? = null

    interface ItemClickListener {
        fun onItemClick(view: View?, position: Int)
    }

    init {
        mData = data
        mInflater = LayoutInflater.from(context)
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = mInflater.inflate(R.layout.row_user, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val itemValue = mData[position]
        holder.tv_companyName.text = itemValue.nameCompany
        holder.tv_distance.text = itemValue.distance.toString()
    }

    override fun getItemCount(): Int {
        return mData.size
    }

    fun setClickListener(itemClickListener: ItemClickListener?) {
        mClickListener = itemClickListener
    }

    inner class ViewHolder internal constructor (itemView: View) :
        RecyclerView.ViewHolder(itemView), View.OnClickListener {

        var tv_companyName: TextView
        var tv_distance: TextView

        override fun onClick(view: View?) {
            if (mClickListener != null) {
                mClickListener!!.onItemClick(view, adapterPosition)
            }
        }

        init {
            tv_companyName = itemView.findViewById(R.id.tv_companyName)
            tv_distance = itemView.findViewById(R.id.tv_distance)
            itemView.setOnClickListener(this)
        }

    }
    fun getItem(id:Int): userViewModel {
        return mData[id]
    }
}