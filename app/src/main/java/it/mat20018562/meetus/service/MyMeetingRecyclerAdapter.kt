package it.mat20018562.meetus.service

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import it.mat20018562.meetus.R
import it.mat20018562.meetus.logDebug
import it.mat20018562.meetus.model.meetingViewModel
import it.mat20018562.meetus.model.serviceModel
import it.mat20018562.meetus.model.serviceViewModel

class MyMeetingRecyclerAdapter internal constructor(context: Context?, data: List<meetingViewModel>)
    : RecyclerView.Adapter<MyMeetingRecyclerAdapter.ViewHolder>() {

    private val mData: List<meetingViewModel>
    private val mInflater: LayoutInflater
    private var mClickListener: ItemClickListener? = null

    interface ItemClickListener {
        fun onItemClick(view: View?, position: Int)
    }

    init {
        mData = data
        mInflater = LayoutInflater.from(context)
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = mInflater.inflate(R.layout.row_meeting, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val itemValue = mData[position]
        //logDebug("Event from adapter: " + itemValue.name)
        holder.tv_creator.text = itemValue.organizerNameSurname
        holder.tv_text.text = itemValue.request
        holder.tv_date.text = itemValue.dateTime
        if(itemValue.approved==1)
        holder.tv_approved.text = "Approvato"
        else
            holder.tv_approved.text = "Da approvare"
        if(itemValue.acceptedQuote==1)
            holder.tv_stateQuote.text = "Preventivo accettato"
        else
            holder.tv_stateQuote.text = "Da approvare"

    }

    override fun getItemCount(): Int {
        return mData.size
    }

    fun setClickListener(itemClickListener: ItemClickListener?) {
        mClickListener = itemClickListener
    }

    inner class ViewHolder internal constructor (itemView: View) :
        RecyclerView.ViewHolder(itemView), View.OnClickListener {

        var tv_creator: TextView
        var tv_text: TextView
        var tv_approved: TextView
        var tv_date: TextView
        var tv_stateQuote: TextView

        override fun onClick(view: View?) {
            if (mClickListener != null) {
                mClickListener!!.onItemClick(view, adapterPosition)
            }
        }

        init {
            tv_creator = itemView.findViewById(R.id.tv_creator)
            tv_text =itemView.findViewById(R.id.tv_text)
            tv_approved=itemView.findViewById(R.id.tv_approved)
            tv_date = itemView.findViewById(R.id.tv_date)
            tv_stateQuote = itemView.findViewById(R.id.tv_stateQuote)
            itemView.setOnClickListener(this)
        }

    }
    fun getItem(id:Int): meetingViewModel {
        return mData[id]
    }
}