package it.mat20018562.meetus.service

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import it.mat20018562.meetus.R
import it.mat20018562.meetus.logDebug
import it.mat20018562.meetus.model.*

class MyPaymentRecyclerAdapter internal constructor(context: Context?, data: List<paymentViewModel>)
    : RecyclerView.Adapter<MyPaymentRecyclerAdapter.ViewHolder>() {

    private val mData: List<paymentViewModel>
    private val mInflater: LayoutInflater
    private var mClickListener: ItemClickListener? = null

    interface ItemClickListener {
        fun onItemClick(view: View?, position: Int)
    }

    init {
        mData = data
        mInflater = LayoutInflater.from(context)
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = mInflater.inflate(R.layout.row_payment, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val itemValue = mData[position]
        holder.tv_datePayment.text = itemValue.date
        if(itemValue.confirmed==1)
            holder.tv_confirmedPayment.text = "Confermato"
        else
            holder.tv_confirmedPayment.text = "Da confermare"
    }

    override fun getItemCount(): Int {
        return mData.size
    }

    fun setClickListener(itemClickListener: ItemClickListener?) {
        mClickListener = itemClickListener
    }

    inner class ViewHolder internal constructor (itemView: View) :
        RecyclerView.ViewHolder(itemView), View.OnClickListener {

        var tv_datePayment: TextView
        var tv_confirmedPayment: TextView
        //var btt_confirm: Button

        override fun onClick(view: View?) {
            if (mClickListener != null) {
                mClickListener!!.onItemClick(view, adapterPosition)
            }
        }

        init {
            tv_datePayment = itemView.findViewById(R.id.tv_datePayment)
            tv_confirmedPayment  = itemView.findViewById(R.id.tv_confirmedPayment)
            //btt_confirm = itemView.findViewById(R.id.btt_confirm)
            itemView.setOnClickListener(this)
        }

    }
    fun getItem(id:Int): paymentViewModel {
        return mData[id]
    }
}