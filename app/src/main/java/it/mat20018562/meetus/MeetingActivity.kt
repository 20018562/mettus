package it.mat20018562.meetus

import android.app.Notification
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.pdf.PdfDocument
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Message
import android.view.View
import android.widget.*
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.internal.ViewUtils.getContentView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import it.mat20018562.meetus.model.*
import it.mat20018562.meetus.service.MyPaymentRecyclerAdapter
import it.mat20018562.meetus.service.MyServiceRecyclerAdapter
import java.text.SimpleDateFormat
import java.time.LocalDateTime
import java.time.LocalTime
import kotlin.time.Duration.Companion.milliseconds
import com.google.android.material.internal.ViewUtils.getContentView
import android.widget.Toast
import java.io.FileOutputStream
import android.os.Environment
import android.os.Environment.getExternalStorageDirectory
import java.io.File
import java.io.IOException
import androidx.core.content.ContextCompat

import android.graphics.Typeface
import android.graphics.pdf.PdfDocument.PageInfo


class MeetingActivity : AppCompatActivity(), MyPaymentRecyclerAdapter.ItemClickListener {

    //private val user = FirebaseAuth.getInstance().currentUser
    private lateinit var currentUser: userViewModel
    private lateinit var event: eventModel
    private lateinit var service: serviceModel
    private lateinit var organizerUser: userModel
    private lateinit var supplierUser: userModel

    private var serviceId: String = ""
    private var eventId: String = ""
    private var supplierUserId: String = ""
    private var id: String = ""

    private val db = FirebaseFirestore.getInstance()

    private var notificationManager: NotificationManager? = null

    private var meeting : meetingModel = meetingModel()

    private var payment : paymentViewModel = paymentViewModel()
    private val paymentList = ArrayList<paymentViewModel>()
    private var adapter: MyPaymentRecyclerAdapter? = null

    //private lateinit var tv_dateTime: TextView
    private lateinit var tv_date: TextView
    private lateinit var tv_time: TextView
    private lateinit var tv_request: TextView
    private lateinit var tv_quote: TextView
    private lateinit var tv_note: TextView
    private lateinit var tv_listPayment: TextView

    private lateinit var sp_paymentMode: Spinner
    private lateinit var sp_paymentMethod: Spinner

    private lateinit var btn_saveMeeting: Button
    private lateinit var btn_acceptMeeting: Button
    private lateinit var btn_acceptQuote: Button
    private lateinit var btn_updateQuote: Button

    private lateinit var btn_sendPayment: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_meeting)
        currentUser = intent.getSerializableExtra("currentUser") as userViewModel

        notificationManager =getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        supplierUserId =intent.getStringExtra("supplierUserId").toString()
        eventId= intent.getStringExtra("eventId").toString()
        serviceId= intent.getStringExtra("serviceId").toString()
        id= intent.getStringExtra("id").toString()

        tv_date = findViewById(R.id.tv_date)
        tv_time = findViewById(R.id.tv_time)
        tv_request = findViewById(R.id.tv_request)
        tv_quote = findViewById(R.id.tv_quote)
        tv_note = findViewById(R.id.tv_note)
        btn_saveMeeting = findViewById(R.id.btn_saveMeeting)
        btn_acceptMeeting = findViewById(R.id.btn_acceptMeeting)
        btn_acceptQuote = findViewById(R.id.btn_acceptQuote)
        btn_updateQuote = findViewById(R.id.btn_updateQuote)
        btn_sendPayment = findViewById(R.id.btn_sendPayment)
        tv_listPayment =  findViewById(R.id.tv_listPayment)

        sp_paymentMode = findViewById(R.id.sp_paymentMode)
        sp_paymentMethod = findViewById(R.id.sp_paymentMethod)

        ArrayAdapter.createFromResource(
            this,
            R.array.paymentModeList,
            android.R.layout.simple_spinner_item
        ).also { adapter ->
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            sp_paymentMode.adapter = adapter
        }

        ArrayAdapter.createFromResource(
            this,
            R.array.paymentMethodList,
            android.R.layout.simple_spinner_item
        ).also { adapter ->
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            sp_paymentMethod.adapter = adapter
        }

        prepareLayout()

        loadRecivleViewPayment()

        btn_saveMeeting.setOnClickListener{
            saveMeeting()
        }

        btn_acceptMeeting.setOnClickListener{
            acceptMeeting()
        }

        btn_acceptQuote.setOnClickListener{
            acceptQuote()
        }
        btn_updateQuote.setOnClickListener{
            updateQuote()
        }

        btn_sendPayment.setOnClickListener{
            sendPayment()
        }
    }

    private fun loadRecivleViewPayment() {
        paymentList.clear()
        db.collection("meeting")
            .document(id)
            .collection("payments")
            .get()
            .addOnSuccessListener { documents ->
                for (document in documents) {
                    /*payment.id = document.id
                    payment.date = document.get("date").toString()
                    payment.confirmed = document.get("confirmed") as Int?*/
                    payment = document.toObject(paymentViewModel::class.java)!!
                    payment.id = document.id
                    //logDebug("Event ID:" + document.id + " Name service: " + serviceTmp.name)
                    paymentList.add(payment)
                }

                val recyclerView = findViewById<RecyclerView>(R.id.rv_payment)
                recyclerView.layoutManager = LinearLayoutManager(this)
                //logDebug("Step1")
                adapter = MyPaymentRecyclerAdapter(this, paymentList)
                adapter!!.setClickListener(this)
                recyclerView.adapter = adapter
                //logDebug("Step2")
            }
            .addOnFailureListener { exception ->
                logError("Error getting documents: ", exception)
            }
    }

    private fun sendPayment() {
        var payment: paymentModel = paymentModel()
        payment.meetingId = id
        payment.date = LocalDateTime.now().toString()
        db.collection("meeting").document(id).collection("payments").document().set(payment)
            .addOnSuccessListener {
                loadRecivleViewPayment()
                sendNotification("it.meetus.personal.${supplierUser?.email}", getString(R.string.paymentToConfirm) )
                Toast.makeText(this, R.string.saveSuccess, Toast.LENGTH_LONG).show()
            }
    }

    private fun updateQuote() {
        meeting.quote = tv_quote.text.toString().toDouble()
        db.collection("meeting").document(id).set(meeting)
            .addOnSuccessListener {
                sendNotification("it.meetus.personal.${supplierUser?.email}", getString(R.string.updatedQuote) )
                Toast.makeText(this, R.string.saveSuccess, Toast.LENGTH_LONG).show()
            }
            .addOnFailureListener { exception ->
                logError(exception.message.toString())
            }
    }

    private fun acceptQuote() {
        event!!.budget = event!!.budget!! - meeting.quote!!
        db.collection("events").document(eventId).set(event!!)
            .addOnSuccessListener {
                val intent = Intent(this,MeetingActivity::class.java)
                intent.putExtra("currentUser",currentUser)
                intent.putExtra("id",id)
                startActivity(intent)
            }
            .addOnFailureListener { exception ->
                logError(exception.message.toString())
            }

        meeting.acceptedQuote = 1
        db.collection("meeting").document(id).set(meeting)
            .addOnSuccessListener {
                sendNotification("it.meetus.personal.${supplierUser?.email}", getString(R.string.acceptedQuote) )
                Toast.makeText(this, R.string.saveSuccess, Toast.LENGTH_LONG).show()
            }
            .addOnFailureListener { exception ->
                logError(exception.message.toString())
            }
    }

    private fun acceptMeeting() {
        meeting.approved = 1
        db.collection("meeting").document(id).set(meeting)
            .addOnSuccessListener {
                sendNotification("it.meetus.personal.${organizerUser?.email}", getString(R.string.acceptedMeeting) )
                Toast.makeText(this, R.string.acceptedMeeting, Toast.LENGTH_LONG).show()
            }
            .addOnFailureListener { exception ->
                logError(exception.message.toString())
            }
    }

    private fun sendNotification(channelID: String, mess: String) {
        //logDebug("Channel " + channelID + " mess " + mess)
        val notificationID = 1
        val resultIntent = Intent(this, LoginActivity::class.java)
        val pendingIntent = PendingIntent.getActivity(
            this,
            0,
            resultIntent,
            PendingIntent.FLAG_UPDATE_CURRENT
        )

        //var channelID = "it.meetus.feed"

        var notification = Notification.Builder(this@MeetingActivity,
            channelID)
            .setContentTitle(mess)
            .setContentText("")
            .setSmallIcon(android.R.drawable.sym_def_app_icon)
            .setChannelId(channelID)
            .setContentIntent(pendingIntent)
            .build()

        notificationManager?.notify(notificationID, notification)
    }

    private fun loadEvent() {
        //logDebug("Load event : " + eventId)
        db.collection("events")
            .document(meeting.EventId!!)
            .get()
            .addOnSuccessListener { document ->
                //logDebug("Service selection success" )
                if (document != null) {
                    eventId = document.id
                    event = document.toObject(eventModel::class.java)!!
                }
            }
            .addOnFailureListener { exception ->
                logError(exception.message.toString())
            }
    }

    private fun saveMeeting() {
        try{
            meeting.date = tv_date.text.toString()
            meeting.time = tv_time.text.toString()
            meeting.request = tv_request.text.toString()


            val timeRequest = LocalTime.parse(meeting.time)
            val timeStartEvent = LocalTime.parse(service.timeStart)
            val timeStopEvent = LocalTime.parse(service.timeStop)


            if(timeRequest.isBefore(timeStartEvent) || timeRequest.isAfter(timeStopEvent)  )
            {
                Toast.makeText(this, R.string.timeNotValid, Toast.LENGTH_LONG).show()
            }else{
                if(id == "new") {
                    meeting.organizerUserId = currentUser.id
                    meeting.supplierUserId = supplierUserId
                    meeting.EventId = eventId
                    meeting.organizerNameSurname = organizerUser!!.name + " " + organizerUser!!.surname
                    db.collection("meeting").document().set(meeting)
                        .addOnSuccessListener {
                            sendNotification("it.meetus.personal.${supplierUser?.email}", "Nuova richiesta di incontro")
                            val intent = Intent(this,OrganizerActivity::class.java)
                            intent.putExtra("currentUser",currentUser)
                            startActivity(intent)
                        }
                        .addOnFailureListener { exception ->
                            logError(exception.message.toString())
                        }
                }else{
                    if(meeting.acceptedQuote==1) {
                        meeting.paymentMethod = sp_paymentMethod.selectedItem.toString()
                        meeting.paymentMode = sp_paymentMode.selectedItem.toString()
                        if(currentUser?.role =="fornitore")
                            tv_note.visibility = View.GONE
                    }

                    db.collection("meeting").document(id).set(meeting)
                        .addOnSuccessListener {
                            //logDebug("Update success")
                            //Toast.makeText(this, R.string.saveSuccess, Toast.LENGTH_LONG).show()
                            val intent = Intent(this,OrganizerActivity::class.java)
                            intent.putExtra("currentUser",currentUser)
                            startActivity(intent)
                        }
                        .addOnFailureListener { exception ->
                            logError(exception.message.toString())
                        }
                }

            }

        } catch (e: Exception) {
            Toast.makeText(this, R.string.saveFailed, Toast.LENGTH_LONG).show()
        }

    }

    private fun prepareLayout() {
        /*db.collection("users")
            .document(user?.uid.toString())
            .get()
            .addOnSuccessListener { document ->
                if (document != null) {*/

                    if(id!="new"){
                        loadMeeting();
                    }else{
                        btn_acceptQuote.visibility = View.GONE
                        tv_listPayment.visibility = View.GONE
                        tv_quote.visibility = View.GONE
                        //logDebug("Meeting new and supId = " + supplierUserId)
                        meeting.supplierUserId = supplierUserId
                        meeting.organizerUserId = currentUser.id
                        loadSupplier()
                        loadOrganizer()
                        loadService()
                    }

                    //currentUser = document.toObject(userModel::class.java)!!

                    if(currentUser!!.role=="organizzatore"){
                        btn_acceptMeeting.visibility = View.GONE
                        tv_quote.isEnabled = false
                        btn_updateQuote.visibility = View.GONE
                    }else{
                        btn_acceptQuote.visibility = View.GONE

                    }
                /*}
            }
            .addOnFailureListener { exception ->
                logError(exception.message.toString())
            }*/
    }

    private fun loadService() {
        //logDebug("ServiceId " + serviceId)
        db.collection("services")
            .document(serviceId)
            .get()
            .addOnSuccessListener { document ->
                //logDebug("Service selection success" )
                if (document != null) {
                    service = document.toObject(serviceModel::class.java)!!
                }
            }
            .addOnFailureListener { exception ->
                logError(exception.message.toString())
            }
    }

    private fun loadMeeting() {
        db.collection("meeting")
            .document(id)
            .get()
            .addOnSuccessListener { document ->
                if (document != null) {
                    meeting = document.toObject(meetingModel::class.java)!!

                    tv_date.text = meeting.date
                    tv_time.text = meeting.time
                    tv_request.text = meeting.request
                    if(meeting.quote!=null)
                        tv_quote.text = meeting.quote.toString()

                    for (i in 0 until sp_paymentMethod.getCount()) {
                        if (sp_paymentMethod.getItemAtPosition(i).toString().equals(meeting.paymentMethod)) {
                            sp_paymentMethod.setSelection(i);
                        }
                    }

                    for (i in 0 until sp_paymentMode.getCount()) {
                        if (sp_paymentMode.getItemAtPosition(i).toString().equals(meeting.paymentMode)) {
                            sp_paymentMode.setSelection(i);
                        }
                    }

                    if(currentUser?.role=="fornitore")
                        btn_saveMeeting.visibility = View.GONE

                    if(meeting.quote==null)
                        btn_acceptQuote.visibility = View.GONE

                    if(meeting.approved==1)
                        btn_acceptMeeting.visibility= View.GONE
                    if(meeting.acceptedQuote==1) {
                        btn_acceptQuote.visibility = View.GONE
                        btn_updateQuote.visibility = View.GONE
                        sp_paymentMethod.visibility = View.VISIBLE
                        sp_paymentMode.visibility = View.VISIBLE
                        if(currentUser?.role =="organizzatore") {
                            //logDebug("View btt send payment")
                            btn_sendPayment.visibility = View.VISIBLE
                        }else{
                            tv_note.visibility = View.VISIBLE
                        }
                    }

                    loadOrganizer()
                    loadSupplier()
                    loadEvent()
                }
            }
            .addOnFailureListener { exception ->
                logError(exception.message.toString())
            }
    }

    private fun loadSupplier() {
        db.collection("users")
            .document(meeting.supplierUserId!!)
            .get()
            .addOnSuccessListener { document ->
                if (document != null) {
                        supplierUser = document.toObject(userModel::class.java)!!
                    }
                }
            .addOnFailureListener { exception ->
                logError(exception.message.toString())
            }
    }

    private fun loadOrganizer() {
        db.collection("users")
            .document(meeting.organizerUserId!!)
            .get()
            .addOnSuccessListener { document ->
                if (document != null) {
                    organizerUser = document.toObject(userModel::class.java)!!
                }
            }
            .addOnFailureListener { exception ->
                logError(exception.message.toString())
            }
    }

    override fun onItemClick(view: View?, position: Int) {
        if(currentUser?.role =="fornitore"){
            var payment: paymentModel = paymentModel()
            var paymentId = adapter!!.getItem(position).id!!
            //logDebug("Payment in confirm: " + adapter!!.getItem(position).id!!)
            db.collection("meeting").document(id).collection("payments").document(paymentId)
                .get()
                .addOnSuccessListener { document ->
                    if (document != null) {
                        payment = document.toObject(paymentModel::class.java)!!
                        payment.confirmed = 1
                        db.collection("meeting").document(id).collection("payments").document(paymentId).set(payment)
                            .addOnSuccessListener {
                                loadRecivleViewPayment()
                                sendNotification("it.meetus.personal.${organizerUser?.email}", getString(R.string.paymentConfirmed) )
                                Toast.makeText(this, R.string.paymentConfirmed, Toast.LENGTH_LONG).show()
                                paymentReceipt(paymentId)
                            }
                    }
                }
                .addOnFailureListener { exception ->
                    logError(exception.message.toString())
                }

        }
    }

    private fun paymentReceipt(paymentId: String) {
        // create a new document
        var document: PdfDocument = PdfDocument();

        // create a page description
        var pageInfo: PdfDocument.PageInfo = PdfDocument.PageInfo.Builder(100, 100, 1).create();

        // start a page
        var page: PdfDocument.Page = document.startPage(pageInfo);

        // draw something on the page
        val paintTxt = Paint()
        paintTxt.setStyle(Paint.Style.FILL);
        paintTxt.setColor(Color.BLACK);
        paintTxt.setTextSize(5F);
        val canvas: Canvas = page.getCanvas()
        //canvas.drawText("Ricevuta di pagamento per ID$paymentId", 209F, 100F, paintTxt);
        canvas.drawText("Ricevuta $paymentId", 10F, 10F, paintTxt);

        // finish the page
        document.finishPage(page);

        val file = File(getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS), "Ricevuta-$paymentId.pdf")
        try {
            // after creating a file name we will
            // write our PDF file to that location.
            document.writeTo(FileOutputStream(file))

            // below line is to print toast message
            // on completion of PDF generation.
            Toast.makeText(this, R.string.receivedGenereted, Toast.LENGTH_LONG).show()
            logDebug(getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS).toString())
            logDebug(file.toURI().toString())
        } catch (e: Exception) {
            logError(e.message.toString())
            Toast.makeText(this, R.string.errorReceived, Toast.LENGTH_LONG).show()
        }
        // after storing our pdf to that
        // location we are closing our PDF file.
        // after storing our pdf to that
        // location we are closing our PDF file.
        document.close()
    }
}