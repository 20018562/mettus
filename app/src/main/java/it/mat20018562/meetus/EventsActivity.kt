package it.mat20018562.meetus

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import it.mat20018562.meetus.model.*
import it.mat20018562.meetus.service.MyEventRecyclerAdapter

class EventsActivity : AppCompatActivity(), MyEventRecyclerAdapter.ItemClickListener  {

    private val eventList = ArrayList<eventViewModel>()
    private var adapter: MyEventRecyclerAdapter? = null

    private lateinit var currentUser: userViewModel

    //private val user = FirebaseAuth.getInstance().currentUser
    private val db = FirebaseFirestore.getInstance()

    private var eventTmp : eventViewModel = eventViewModel()
    private var inviteTmp : eventInvitationModel = eventInvitationModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_events)

        currentUser = intent.getSerializableExtra("currentUser") as userViewModel

        //logDebug("Load activity services from serviceS")

        /*db.collection("users")
            .document(user?.uid.toString())
            .get()
            .addOnSuccessListener { document ->
                if (document != null) {
                    var currentUser = document.toObject(userModel::class.java)!!*/
                    //uid = document.id
                    LoadEvent(currentUser)
            /*    }
            }
            .addOnFailureListener { exception ->
                logError(exception.message.toString())
            }*/
    }

    fun LoadEvent(currentUser: userViewModel) {

        if(currentUser.role=="invitato"){

            db.collection("invitationEvent")
                .whereEqualTo("email", currentUser.email)
                .get()
                .addOnSuccessListener { documents ->
                    //logDebug("Load invite for user : " )
                    for (document in documents) {
                        //logDebug("Invite : " + document.id)
                        inviteTmp = document.toObject(eventInvitationModel::class.java)
                        eventTmp = eventViewModel()
                        eventTmp.typeEvent = inviteTmp.name
                        eventTmp.id = inviteTmp.eventId
                        eventList.add(eventTmp)
                    }

                    val recyclerView = findViewById<RecyclerView>(R.id.rv_events)
                    recyclerView.layoutManager = LinearLayoutManager(this)
                    //logDebug("Step1")
                    adapter = MyEventRecyclerAdapter(this, eventList)
                    adapter!!.setClickListener(this)
                    recyclerView.adapter = adapter
                    //logDebug("Step2")
                }
                .addOnFailureListener { exception ->
                    logError("Error getting documents: ", exception)
                }


        }else{
            db.collection("events")
                .whereEqualTo("userIdCreator", currentUser.id)
                .get()
                .addOnSuccessListener { documents ->
                    //logDebug("Events : LOAD EVENTS")
                    for (document in documents) {
                        eventTmp = document.toObject(eventViewModel::class.java)
                        eventTmp.id = document.id
                        //logDebug("Events :" + eventTmp.id)
                        eventList.add(eventTmp)
                    }

                    val recyclerView = findViewById<RecyclerView>(R.id.rv_events)
                    recyclerView.layoutManager = LinearLayoutManager(this)
                    //logDebug("Step1")
                    adapter = MyEventRecyclerAdapter(this, eventList)
                    adapter!!.setClickListener(this)
                    recyclerView.adapter = adapter
                    //logDebug("Step2")
                }
                .addOnFailureListener { exception ->
                    logError("Error getting documents: ", exception)
                }
        }


    }

    override fun onItemClick(view: View?, position: Int) {
        val intent = Intent(this,EventActivity::class.java)
        intent.putExtra("id",adapter!!.getItem(position).id)
        intent.putExtra("currentUser",currentUser)
        startActivity(intent)
    }
}