package it.mat20018562.meetus

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import it.mat20018562.meetus.model.serviceViewModel
import it.mat20018562.meetus.model.userViewModel
import it.mat20018562.meetus.service.MyFeedRecyclerAdapter
import it.mat20018562.meetus.service.MyServiceRecyclerAdapter

class SupplierListForServiceInEventActivity : AppCompatActivity(), MyServiceRecyclerAdapter.ItemClickListener {

    private lateinit var currentUser: userViewModel

    private val servicesList = ArrayList<serviceViewModel>()
    private var adapter: MyServiceRecyclerAdapter? = null

    //private val user = FirebaseAuth.getInstance().currentUser
    private val db = FirebaseFirestore.getInstance()

    private var serviceTmp : serviceViewModel = serviceViewModel()

    private var eventId: String? =null
    private var serviceType: String? =null
    private var supplyerId: String? =null



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_supplier_list_fror_service_in_event)

        currentUser = intent.getSerializableExtra("currentUser") as userViewModel

        eventId= intent.getStringExtra("eventId").toString()
        //serviceType= intent.getStringExtra("serviceType").toString()
        supplyerId= intent.getStringExtra("supplyerId").toString()
        //logDebug("Choose service-supplier for organizer cat:" + serviceType)
        db.collection("services")
            .whereEqualTo("userId", supplyerId)
            .get()
            .addOnSuccessListener { documents ->
                //logDebug("Success get service")
                for (document in documents) {
                    serviceTmp = document.toObject(serviceViewModel::class.java)
                    serviceTmp.id = document.id
                    //logDebug("ServiceTmp ID:" + serviceTmp.id)
                    servicesList.add(serviceTmp)
                }

                val recyclerView = findViewById<RecyclerView>(R.id.rv_service)
                recyclerView.layoutManager = LinearLayoutManager(this)
                //logDebug("Step1")
                adapter = MyServiceRecyclerAdapter(this, servicesList)
                adapter!!.setClickListener(this)
                recyclerView.adapter = adapter
                //logDebug("Step2")
            }
            .addOnFailureListener { exception ->
                logError("Error getting documents: ", exception)
            }
    }

    override fun onItemClick(view: View?, position: Int) {

        val intent = Intent(this,ServiceActivity::class.java)
        //logDebug("Id service selected from SupplistService: " + adapter!!.getItem(position).id)
        intent.putExtra("id",adapter!!.getItem(position).id)
        intent.putExtra("currentUser",currentUser)
        intent.putExtra("eventId",eventId)
        startActivity(intent)
    }
}

