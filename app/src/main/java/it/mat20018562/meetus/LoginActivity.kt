package it.mat20018562.meetus

import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Patterns
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import it.mat20018562.meetus.model.eventModel
import it.mat20018562.meetus.model.userModel
import it.mat20018562.meetus.model.userViewModel

class LoginActivity : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth
    private lateinit var db: FirebaseFirestore

    private lateinit var currentUser: userViewModel


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        auth = FirebaseAuth.getInstance()
        db = FirebaseFirestore.getInstance()

        val btnSignUp = findViewById<Button>(R.id.btn_sign_up)
        btnSignUp.setOnClickListener{
            startActivity(Intent(this, SignupActivity::class.java))
        }
        val btnLogIn = findViewById<Button>(R.id.btn_log_in)
        btnLogIn.setOnClickListener{
            loginInUser()
        }
    }

    private fun loginInUser(){
        val tvUsername = findViewById<TextView>(R.id.tv_username)
        if (tvUsername.text.toString().isEmpty()) {
            tvUsername.error = getString(R.string.enterMail)
            tvUsername.requestFocus()
            return
        }

        if (!Patterns.EMAIL_ADDRESS.matcher(tvUsername.text.toString()).matches()) {
            tvUsername.error = getString(R.string.errorMail)
            tvUsername.requestFocus()
            return
        }
        val tvPassword = findViewById<TextView>(R.id.tv_password)
        if (tvPassword.text.toString().isEmpty()) {
            tvPassword.error = getString(R.string.enterPwd)
            tvPassword.requestFocus()
            return
        }
        //logDebug("init auth");
        auth.signInWithEmailAndPassword(tvUsername.text.toString(), tvPassword.text.toString())
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    //logDebug("auth success")
                    lateinit var role : String

                    db.collection("users")
                        .document(auth.uid.toString())
                        .get()
                        .addOnSuccessListener { document ->
                                if (document != null) {
                                    //val user = FirebaseAuth.getInstance().currentUser

                                    currentUser = document.toObject(userViewModel::class.java)!!
                                    currentUser.id = document.id
                                    if(currentUser.role=="organizzatore"){
                                        val intent = Intent(this,OrganizerActivity::class.java)
                                        intent.putExtra("currentUser",currentUser)
                                        startActivity(intent)
                                    }else if(currentUser.role=="fornitore"){
                                        logDebug("Start activity fornitore")
                                        val intent = Intent(this,SupplierActivity::class.java)
                                        intent.putExtra("currentUser",currentUser)
                                        startActivity(intent)
                                    }else if(currentUser.role=="invitato"){

                                        if(currentUser.editProfile==1){
                                            val intent = Intent(this,ProfileActivity::class.java)
                                            intent.putExtra("currentUser",currentUser)
                                            startActivity(intent)
                                        }else{
                                            val intent = Intent(this,GuestActivity::class.java)
                                            intent.putExtra("currentUser",currentUser)
                                            startActivity(intent)
                                        }
                                    }

                                } else {
                                    Toast.makeText(baseContext, getString(R.string.authFailed), Toast.LENGTH_SHORT).show()
                                }
                            }
                        .addOnFailureListener {
                            Toast.makeText(baseContext, getString(R.string.authFailed), Toast.LENGTH_SHORT).show()
                        }
                } else {
                    //logDebug("auth failed")
                    Toast.makeText(baseContext, getString(R.string.authFailed), Toast.LENGTH_SHORT).show()
                }
            }
    }
}