package it.mat20018562.meetus

import android.Manifest
import android.R.attr
import android.app.Activity
import android.content.ContentValues
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.view.View
import android.widget.*
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import it.mat20018562.meetus.model.serviceModel
import it.mat20018562.meetus.model.userModel
import android.widget.Spinner
import android.graphics.Bitmap
import android.R.attr.bitmap
import android.graphics.BitmapFactory
import android.opengl.Visibility
import android.util.Base64
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import it.mat20018562.meetus.model.servicePhotoModel
import it.mat20018562.meetus.model.userViewModel
import it.mat20018562.meetus.service.MyServicePhotoRecyclerAdapter
import java.io.ByteArrayOutputStream
import java.lang.Exception
import java.util.*
import kotlin.collections.ArrayList


class ServiceActivity : AppCompatActivity(), MyServicePhotoRecyclerAdapter.ItemClickListener   {

    private val photos = ArrayList<servicePhotoModel>()
    private var adapter: MyServicePhotoRecyclerAdapter? = null
    private var photoTmp = servicePhotoModel()

    private val PERMISSION_CODE = 1000
    private val IMAGE_CAPTURE_CODE = 1001

    private var image_uri: Uri? = null

    private lateinit var currentUser: userViewModel
    //private val user = FirebaseAuth.getInstance().currentUser
    //private var currentUser: userModel = userModel()

    private val db = FirebaseFirestore.getInstance()

    private var service: serviceModel = serviceModel()

    private var serviceId: String = ""
    private var eventId: String? =null

    /*private lateinit var tv_userId: TextView*/
    private lateinit var tv_name: TextView
    private lateinit var tv_description: TextView
    private lateinit var sp_category: Spinner
    //private lateinit var tv_timetables: TextView
    private lateinit var tv_timeStart: TextView
    private lateinit var tv_timeStop: TextView
    private lateinit var tv_price: TextView


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_service)

        serviceId= intent.getStringExtra("id").toString()
        eventId= intent.getStringExtra("eventId").toString()
        currentUser = intent.getSerializableExtra("currentUser") as userViewModel

        tv_name = findViewById(R.id.tv_name)
        tv_description = findViewById(R.id.tv_description)
        sp_category = findViewById(R.id.sp_category)
        //tv_timetables = findViewById(R.id.tv_timetables)
        tv_timeStart = findViewById(R.id.tv_timeStart)
        tv_timeStop = findViewById(R.id.tv_timeStop)
        tv_price = findViewById(R.id.tv_price)

        val btnAddPhoto = findViewById<Button>(R.id.btn_addPhoto)
        val btnSaveService = findViewById<Button>(R.id.btn_saveService)
        val btnRequestMeeting = findViewById<Button>(R.id.btn_requestMeeting)

        /*db.collection("users")
            .document(user?.uid.toString())
            .get()
            .addOnSuccessListener { document ->
                if (document != null) {
                    currentUser = document.toObject(userModel::class.java)!!*/

                    if(currentUser.role=="organizzatore"){
                        btnAddPhoto.visibility = View.GONE
                        btnSaveService.visibility = View.GONE

                    }else if(currentUser.role == "fornitore"){
                        btnRequestMeeting.visibility = View.GONE
                    }
                /*}
        }
        .addOnFailureListener { exception ->
            logError(exception.message.toString())
        }*/

        ArrayAdapter.createFromResource(
            this,
            R.array.categoryList,
            android.R.layout.simple_spinner_item
        ).also { adapter ->
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            sp_category.adapter = adapter
        }

        btnSaveService.setOnClickListener{
            saveService()
        }

        btnRequestMeeting.setOnClickListener{
            val intent = Intent(this,MeetingActivity::class.java)
            intent.putExtra("serviceId",serviceId)
            intent.putExtra("eventId",eventId)
            intent.putExtra("supplierUserId",service.userId)
            intent.putExtra("id","new")
            intent.putExtra("currentUser",currentUser)
            startActivity(intent)
        }

        //serviceId= intent.getStringExtra("id").toString()
        //logDebug("ServiceID from service:$serviceId")

        if(serviceId=="new"){
            service.userId = currentUser.id
            btnAddPhoto.visibility = View.GONE
        }else{
            db.collection("services")
                .document(serviceId)
                .get()
                .addOnSuccessListener { document ->
                    //logDebug("Service selection success" )
                    if (document != null) {
                        service = document.toObject(serviceModel::class.java)!!

                        tv_name.text = service.name
                        tv_description.text = service.description
                        //tv_timetables.text = service.timetables
                        tv_timeStart.text = service.timeStart
                        tv_timeStop.text = service.timeStop
                        tv_price.text = service.price?.toString()

                        for (i in 0 until sp_category.getCount()) {
                            if (sp_category.getItemAtPosition(i).toString().equals(service.category)) {
                                sp_category.setSelection(i);
                            }
                        }
                    }
                }
                .addOnFailureListener { exception ->
                    logError(exception.message.toString())
                }

        }


        btnAddPhoto.setOnClickListener {
            //if system os is Marshmallow or Above, we need to request runtime permission
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
                if (checkSelfPermission(Manifest.permission.CAMERA)
                    == PackageManager.PERMISSION_DENIED ||
                    checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_DENIED){
                    //permission was not enabled
                    val permission = arrayOf(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    //show popup to request permission
                    requestPermissions(permission, PERMISSION_CODE)
                }
                else{
                    //permission already granted
                    openCamera()
                }
            }
            else{
                //system os is < marshmallow
                openCamera()
            }
        }

        loadPhoto();

    }

    private fun loadPhoto() {
        photos.clear()
        db.collection("services")
            .document(serviceId)
            .collection("photos")
            .get()
            .addOnSuccessListener { documents ->
                try {
                    //logDebug("Success get photo service: " + documents.count().toString())
                    for (document in documents) {
                        photoTmp = document.toObject(servicePhotoModel::class.java)
                        photoTmp.id  = document.id
                        //logDebug("photo " + document.id)
                        photos.add(photoTmp)
                        //logDebug("Photo added")
                    }

                    val recyclerView = findViewById<RecyclerView>(R.id.rv_photo)
                    recyclerView.layoutManager = LinearLayoutManager(this)

                    adapter = MyServicePhotoRecyclerAdapter(this, photos)
                    adapter!!.setClickListener(this)
                    recyclerView.adapter = adapter
                }catch (e : Exception){
                    logError(e.message.toString())
                }
            }
            .addOnFailureListener { exception ->
                logError("Error getting documents: ", exception)
            }
    }


    override fun onItemClick(view: View?, position: Int) {
       /* val intent = Intent(this,ServiceActivity::class.java)
        intent.putExtra("id",adapter!!.getItem(position).id)
        startActivity(intent)*/
    }

    private fun saveService() {

        service.name = tv_name.text.toString()
        service.description = tv_description.text.toString()
        service.companyName = currentUser.nameCompany
        service.category = sp_category.selectedItem.toString()
        //service.timetables = tv_timetables.text.toString()
        service.timeStart = tv_timeStart.text.toString()
        service.timeStop = tv_timeStop.text.toString()
        service.price = tv_price.text.toString().toDouble()
        if(serviceId == "new") {
            db.collection("services").document().set(service)
                .addOnSuccessListener {
                    UpdateUserCategory()
                    //Toast.makeText(this, R.string.saveSuccess, Toast.LENGTH_LONG).show()
                }
                .addOnFailureListener { exception ->
                    Toast.makeText(this, R.string.saveFailed, Toast.LENGTH_LONG).show()
                    logError(exception.message.toString())
                }
        }else{
            db.collection("services").document(serviceId).set(service)
                .addOnSuccessListener {
                    //Toast.makeText(this, R.string.saveSuccess, Toast.LENGTH_LONG).show()
                    UpdateUserCategory()
                    //intent.putExtra("id",adapter!!.getItem(position).id)
                }
                .addOnFailureListener { exception ->
                    Toast.makeText(this, R.string.saveFailed, Toast.LENGTH_LONG).show()
                    logError(exception.message.toString())
                }
        }
        val intent = Intent(this,ServicesActivity::class.java)
        intent.putExtra("currentUser",currentUser)
        startActivity(intent)
    }

    private fun UpdateUserCategory() {
        var userCategory= ""
        logDebug("update categoryService for user : " + service.userId)
        db.collection("services").whereEqualTo("userId",service.userId)
            .get()
            .addOnSuccessListener { documents ->
                //logDebug("Success get service")
                for (document in documents) {
                    if(userCategory.contains(document.get("category").toString())==false){
                        userCategory+=document.get("category").toString()+ ";"
                    }
                }
                currentUser.categoryService = userCategory
                db.collection("users").document(service.userId!!).set(currentUser)
                    .addOnSuccessListener {
                        logDebug("categoryService updated")
                    }
                    .addOnFailureListener { exception ->
                        logError(exception.message.toString())
                    }

            }
            .addOnFailureListener { exception ->
                logError("Error getting documents: ", exception)
            }
    }

    private fun openCamera() {
        val values = ContentValues()
        values.put(MediaStore.Images.Media.TITLE, "New Picture")
        values.put(MediaStore.Images.Media.DESCRIPTION, "From the Camera")
        image_uri = contentResolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values)
        //camera intent
        val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, image_uri)
        startActivityForResult(cameraIntent, IMAGE_CAPTURE_CODE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        //called when image was captured from camera intent
        if (resultCode == Activity.RESULT_OK){
            //set image captured to image view
            //image_view.setImageURI(image_uri)
                /*
            var imgBase64 = uriToBase64(image_uri!!)
            db.collection("services").document(serviceId).collection("photos").document().set(mapOf("base64" to imgBase64))
                .addOnSuccessListener {
                    Toast.makeText(this, R.string.saveSuccess, Toast.LENGTH_LONG).show()
                }
                .addOnFailureListener { exception ->
                    logError(exception.message.toString())
                }

                 */
            //logDebug(imgBase64)


            /*LOAD TO FIRESTORE*/
            var docId = UUID.randomUUID().toString()
            db.collection("services").document(serviceId).collection("photos").document(docId).set(mapOf("type" to "photoService", "serviceId" to serviceId))
                .addOnSuccessListener {

                    val image: StorageReference =
                        FirebaseStorage.getInstance().reference.child("images/$serviceId/$docId")
                    image.putFile(image_uri!!).addOnSuccessListener {
                        Toast.makeText(this, "Image Uploaded.", Toast.LENGTH_SHORT).show()
                        loadPhoto()
                    }.addOnFailureListener {
                        Toast.makeText(this, "Upload Failed.", Toast.LENGTH_SHORT).show()
                    }
                    Toast.makeText(this, R.string.saveSuccess, Toast.LENGTH_LONG).show()

                }
                .addOnFailureListener { exception ->
                    logError(exception.message.toString())
                }




            /*--------------------*/
        }
    }

    // Saved Broken Image
    /*private fun uriToBase64(imageUri: Uri): String {
        val input = this.contentResolver.openInputStream(imageUri)
        //val bm = BitmapFactory.decodeResource(resources, R.drawable.test)
        val image = BitmapFactory.decodeStream(input, null, null)
        //val imageResize = Bitmap.createScaledBitmap(image!!,10,10,false)
        //encode image to base64 string
        val baos = ByteArrayOutputStream()
        //bm.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        image!!.compress(Bitmap.CompressFormat.JPEG, 20, baos)
        var imageBytes = baos.toByteArray()

        return Base64.encodeToString(imageBytes, Base64.DEFAULT)
    }*/

}