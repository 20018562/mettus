package it.mat20018562.meetus

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.gms.maps.model.LatLng
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.maps.android.SphericalUtil
import it.mat20018562.meetus.model.*
import it.mat20018562.meetus.service.MyMeetingRecyclerAdapter
import it.mat20018562.meetus.service.MySuppliersRecyclerAdapter

class SuppliersActivity : AppCompatActivity(), MySuppliersRecyclerAdapter.ItemClickListener {


    private lateinit var currentUser: userViewModel
    private lateinit var event: eventModel

    //private val user = FirebaseAuth.getInstance().currentUser
    private val db = FirebaseFirestore.getInstance()

    private var adapter: MySuppliersRecyclerAdapter? = null

    private var supplyer: userViewModel = userViewModel()

    private var eventId: String? =null
    private var serviceType: String? =null

    private val supplyerList = ArrayList<userViewModel>()
    private val supplyerListFiltered = ArrayList<userViewModel>()
    private val markerList = ArrayList<markerModel>()


    private lateinit var btnViewMap:Button
    private lateinit var btnFilter:Button
    private lateinit var maxDistance: EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_suppliers)

        currentUser = intent.getSerializableExtra("currentUser") as userViewModel
        eventId= intent.getStringExtra("eventId").toString()
        serviceType= intent.getStringExtra("serviceType").toString()

        loadEvent()

        btnViewMap = findViewById(R.id.btnViewMap)
        btnViewMap.setOnClickListener{
            viewMap()
        }

        maxDistance = findViewById(R.id.maxDistance)
        btnFilter = findViewById(R.id.btnFilter)
        btnFilter.setOnClickListener{
            filterSupplyer()
        }
    }

    private fun filterSupplyer() {
        try{
            var maxdistance = maxDistance.text.toString().toDouble()
            supplyerListFiltered.clear()
            for(sup in supplyerList)
            {
                if(sup.distance!! < maxdistance){
                    supplyerListFiltered.add(sup)
                }
            }
            val recyclerView = findViewById<RecyclerView>(R.id.rv_suppliers)
            recyclerView.layoutManager = LinearLayoutManager(this)
            //logDebug("Step1")
            adapter = MySuppliersRecyclerAdapter(this, supplyerListFiltered)
            adapter!!.setClickListener(this)
            recyclerView.adapter = adapter
        }catch (ex:Exception){
            Toast.makeText(this,R.string.filterFailed, Toast.LENGTH_LONG).show()
        }

    }


    private fun viewMap() {
        val intent = Intent(this,MapsActivity::class.java)
        val markerList = ArrayList<markerModel>()
        for(sup in supplyerList){
            markerList.add(markerModel(sup.latitude!!, sup.longitude!!, sup.nameCompany!!))
        }
        intent.putExtra("markerList",markerList)
        intent.putExtra("currentUser",currentUser)
        startActivity(intent)
    }

    override fun onItemClick(view: View?, position: Int) {
        val intent = Intent(this,SupplierListForServiceInEventActivity::class.java)
        //val intent = Intent(this,SupplierActivity::class.java)
        //intent.putExtra("serviceType",adapter!!.getItem(position).name)
        intent.putExtra("eventId",eventId)
        intent.putExtra("supplyerId",adapter!!.getItem(position).id)
        intent.putExtra("currentUser",currentUser)
        startActivity(intent)
    }

    private fun loadEvent(){
        //logDebug("Select event " + eventId)
        db.collection("events")
            .document(eventId!!)
            .get()
            .addOnSuccessListener { document ->
                //logDebug("Service selection success" )
                if (document != null) {
                    event = document.toObject(eventModel::class.java)!!
                    loadSupplyers()
                }
            }
            .addOnFailureListener { exception ->
                logError(exception.message.toString())
            }
    }

    private fun loadSupplyers() {
        db.collection("users")
            .whereEqualTo("role", "fornitore")
            .get()
            .addOnSuccessListener { documents ->
                //logDebug("Success get meet for userid: " + user!!.uid + " ")
                for (document in documents) {
                    //logDebug("Meet ID:" + meeting.id)
                    supplyer = document.toObject(userViewModel::class.java)
                    if(supplyer.categoryService?.contains(serviceType!!) == true) {
                        supplyer.distance = SphericalUtil.computeDistanceBetween(LatLng(supplyer.latitude!!, supplyer.longitude!!), LatLng(event.latitude!!, event.longitude!!));
                        supplyer.id = document.id
                        supplyerList.add(supplyer)
                    }
                }

                val recyclerView = findViewById<RecyclerView>(R.id.rv_suppliers)
                recyclerView.layoutManager = LinearLayoutManager(this)
                //logDebug("Step1")
                adapter = MySuppliersRecyclerAdapter(this, supplyerList)
                adapter!!.setClickListener(this)
                recyclerView.adapter = adapter
                //logDebug("Step2")
            }
            .addOnFailureListener { exception ->
                logError("Error getting documents: ", exception)
            }
    }
}