package it.mat20018562.meetus

import android.Manifest
import android.app.Activity
import android.content.ContentValues
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.view.View
import android.widget.Button
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import it.mat20018562.meetus.model.servicePhotoModel
import it.mat20018562.meetus.model.userViewModel
import it.mat20018562.meetus.service.MyServicePhotoRecyclerAdapter
import java.lang.Exception
import java.util.*
import kotlin.collections.ArrayList

class PhotoEventActivity : AppCompatActivity(),MyServicePhotoRecyclerAdapter.ItemClickListener  {

    private val photos = ArrayList<servicePhotoModel>()
    private var adapter: MyServicePhotoRecyclerAdapter? = null
    private var photoTmp = servicePhotoModel()

    private val PERMISSION_CODE = 1000
    private val IMAGE_CAPTURE_CODE = 1001

    private var image_uri: Uri? = null

    private lateinit var currentUser: userViewModel
    private val db = FirebaseFirestore.getInstance()

    private var eventId: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_photo_event)

        currentUser = intent.getSerializableExtra("currentUser") as userViewModel
        //logDebug("User from photo: " + currentUser.id)
        eventId= intent.getStringExtra("eventId").toString()

        val btnAddPhoto = findViewById<Button>(R.id.btn_addPhoto)
        btnAddPhoto.setOnClickListener {
            //if system os is Marshmallow or Above, we need to request runtime permission
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
                if (checkSelfPermission(Manifest.permission.CAMERA)
                    == PackageManager.PERMISSION_DENIED ||
                    checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_DENIED){
                    logDebug("No permission camera")

                    //permission was not enabled
                    val permission = arrayOf(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    //show popup to request permission
                    requestPermissions(permission, PERMISSION_CODE)
                }
                else{
                    //permission already granted
                    openCamera()
                }
            }
            else{
                //system os is < marshmallow
                openCamera()
            }
        }

        loadPhoto()

    }

    private fun loadPhoto() {
        photos.clear()
        db.collection("events")
            .document(eventId)
            .collection("photos")
            .get()
            .addOnSuccessListener { documents ->
                try {
                    //logDebug("Success get photo event: " + documents.count().toString())
                    for (document in documents) {
                        photoTmp = document.toObject(servicePhotoModel::class.java)
                        photoTmp.serviceId = document.get("eventId").toString()
                        photoTmp.id  = document.id
                        //logDebug("photo " + document.id)
                        photos.add(photoTmp)
                        //logDebug("Photo added")
                    }

                    val recyclerView = findViewById<RecyclerView>(R.id.rv_photo)
                    recyclerView.layoutManager = LinearLayoutManager(this)

                    adapter = MyServicePhotoRecyclerAdapter(this, photos)
                    adapter!!.setClickListener(this)
                    recyclerView.adapter = adapter
                }catch (e : Exception){
                    logError(e.message.toString())
                }
            }
            .addOnFailureListener { exception ->
                logError("Error getting documents: ", exception)
            }
    }

    override fun onItemClick(view: View?, position: Int) {
        /* val intent = Intent(this,ServiceActivity::class.java)
         intent.putExtra("id",adapter!!.getItem(position).id)
         startActivity(intent)*/
    }

    private fun openCamera() {
        val values = ContentValues()
        values.put(MediaStore.Images.Media.TITLE, "New Picture")
        values.put(MediaStore.Images.Media.DESCRIPTION, "From the Camera")
        image_uri = contentResolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values)
        //camera intent
        val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, image_uri)
        startActivityForResult(cameraIntent, IMAGE_CAPTURE_CODE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        //called when image was captured from camera intent
        if (resultCode == Activity.RESULT_OK){

            /*LOAD TO FIRESTORE*/
            var docId = UUID.randomUUID().toString()
            db.collection("events").document(eventId).collection("photos").document(docId).set(mapOf("type" to "photoEvent", "eventId" to eventId))
                .addOnSuccessListener {

                    val image: StorageReference =
                        FirebaseStorage.getInstance().reference.child("images/$eventId/$docId")
                    image.putFile(image_uri!!).addOnSuccessListener {
                        Toast.makeText(this, R.string.ImageUploaded, Toast.LENGTH_SHORT).show()
                        loadPhoto()
                    }.addOnFailureListener {
                        Toast.makeText(this, R.string.UploadFailed, Toast.LENGTH_SHORT).show()
                    }
                    Toast.makeText(this, R.string.saveSuccess, Toast.LENGTH_LONG).show()

                }
                .addOnFailureListener { exception ->
                    logError(exception.message.toString())
                }
        }
    }
}