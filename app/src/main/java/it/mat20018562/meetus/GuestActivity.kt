package it.mat20018562.meetus

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.widget.Toolbar
import androidx.core.app.ActivityCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.gms.maps.model.LatLng
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.ktx.Firebase
import com.google.maps.android.SphericalUtil
import it.mat20018562.meetus.model.eventInvitationModel
import it.mat20018562.meetus.model.eventViewModel
import it.mat20018562.meetus.model.userModel
import it.mat20018562.meetus.model.userViewModel
import it.mat20018562.meetus.service.MyEventRecyclerAdapter
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationListener
import com.google.android.gms.location.LocationServices


class GuestActivity : AppCompatActivity(), MyEventRecyclerAdapter.ItemClickListener  {

    private val eventList = ArrayList<eventViewModel>()
    private var adapter: MyEventRecyclerAdapter? = null

    private lateinit var currentUser: userViewModel

    private val db = FirebaseFirestore.getInstance()

    private var eventTmp : eventViewModel = eventViewModel()
    private var inviteTmp : eventInvitationModel = eventInvitationModel()

    private var latitude: Double = 0.0;
    private var longitude: Double = 0.0;

    //private lateinit var fusedLocationClient: FusedLocationProviderClient

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_guest)
        //fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        currentUser = intent.getSerializableExtra("currentUser") as userViewModel

        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar);
        /*
        db.collection("users")
            .document(currentUser.id)
            .get()
            .addOnSuccessListener { document ->
                if (document != null) {
                    var currentUser = document.toObject(userModel::class.java)!!
                    */
                    LoadEvents(currentUser);
                /*}
            }
            .addOnFailureListener { exception ->
                logError(exception.message.toString())
            }*/
        /*if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return
        }
        fusedLocationClient.lastLocation
            .addOnSuccessListener { location : Location? ->
                // Got last known location. In some rare situations this can be null.
                latitude = location!!.latitude
                longitude = location!!.longitude
                LoadEvents(currentUser);
            }*/





    }



    fun LoadEvents(currentUser: userViewModel) {
        db.collection("invitationEvent")
            .whereEqualTo("email", currentUser.email)
            .get()
            .addOnSuccessListener { documents ->
                //logDebug("Load invite for user : " )
                for (document in documents) {
                    //logDebug("Invite : " + document.id)
                    inviteTmp = document.toObject(eventInvitationModel::class.java)
                    eventTmp = eventViewModel()
                    eventTmp.typeEvent = inviteTmp.name
                    eventTmp.id = inviteTmp.eventId
                    /*logDebug("1" + eventTmp.latitude!!.toString())
                    logDebug("2" + eventTmp.longitude!!.toString())
                    logDebug("3" + latitude!!.toString())
                    logDebug("4" + latitude!!.toString())*/
                    //eventTmp.distance = SphericalUtil.computeDistanceBetween(LatLng(eventTmp.latitude!!, eventTmp.longitude!!), LatLng(latitude, longitude));
                    eventList.add(eventTmp)
                }

                val recyclerView = findViewById<RecyclerView>(R.id.rv_events)
                recyclerView.layoutManager = LinearLayoutManager(this)
                //logDebug("Step1")
                adapter = MyEventRecyclerAdapter(this, eventList)
                adapter!!.setClickListener(this)
                recyclerView.adapter = adapter
                //logDebug("Step2")
            }
            .addOnFailureListener { exception ->
                logError("Error getting documents: ", exception)
            }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.guest_menu, menu);
        return true;
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId) {
            R.id.logout -> {
                Firebase.auth.signOut()
                startActivity(Intent(this,LoginActivity::class.java))
            }
            R.id.events -> {
                val intent = Intent(this,EventsActivity::class.java)
                intent.putExtra("currentUser",currentUser)
                startActivity(intent)
            }
            R.id.profile -> {
                val intent = Intent(this,ProfileActivity::class.java)
                intent.putExtra("currentUser",currentUser)
                startActivity(intent)
            }

        }
        return super.onOptionsItemSelected(item)
    }

    override fun onItemClick(view: View?, position: Int) {
        val intent = Intent(this,EventActivity::class.java)
        intent.putExtra("id",adapter!!.getItem(position).id)
        intent.putExtra("currentUser",currentUser)
        startActivity(intent)
    }
}