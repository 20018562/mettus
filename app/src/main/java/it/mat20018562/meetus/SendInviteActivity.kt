package it.mat20018562.meetus

import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.ktx.Firebase
import it.mat20018562.meetus.model.eventInvitationModel
import it.mat20018562.meetus.model.eventModel
import it.mat20018562.meetus.model.userModel
import it.mat20018562.meetus.model.userViewModel

class SendInviteActivity : AppCompatActivity() {

    private var auth: FirebaseAuth = Firebase.auth
    private lateinit var currentUser: userViewModel
    private val db = FirebaseFirestore.getInstance()

    //private var event: eventModel = eventModel()

    private var eventId: String = ""

    private lateinit var btn_sendInvite: Button
    private lateinit var tv_email: TextView
    private var pwd: String = "123456"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_send_invite)

        eventId= intent.getStringExtra("eventId").toString()
        currentUser = intent.getSerializableExtra("currentUser") as userViewModel

        tv_email  = findViewById<TextView>(R.id.tv_email)

        btn_sendInvite = findViewById<Button>(R.id.btn_sendInvite)
        btn_sendInvite.setOnClickListener{
            sendInvite()
        }

    }

    private fun sendInvite(){

        //var pwd = getRandomString(10)
        try{
            auth.createUserWithEmailAndPassword(tv_email.text.toString(), pwd)
                .addOnCompleteListener(this) { task ->
                    if (task.isSuccessful) {
                        var usrTmp = userModel()
                        usrTmp.role = "invitato"
                        usrTmp.email = tv_email.text.toString()
                        usrTmp.editProfile = 1
                        db.collection("users").document(this.auth.uid.toString())
                            .set(usrTmp)
                            .addOnSuccessListener {
                                addUserToEvent()
                            }
                            .addOnFailureListener { exception ->
                                Toast.makeText(baseContext, getString(R.string.regFailed), Toast.LENGTH_SHORT).show()
                                logError(exception.message.toString())
                            }
                    } else {
                        if(task.exception?.message.toString() == "The email address is already in use by another account.")
                        {
                            addUserToEvent()
                        }
                        else
                        {
                            Toast.makeText(baseContext, getString(R.string.regFailed), Toast.LENGTH_SHORT).show()
                        }
                    }
                }
        }catch(ex: Exception) {
            Toast.makeText(baseContext, getString(R.string.regFailed), Toast.LENGTH_SHORT).show()
            logError(ex.message.toString())
        }

    }

    private fun addUserToEvent() {
        db.collection("events")
            .document(eventId)
            .get()
            .addOnSuccessListener { document ->
                if (document != null) {
                    var event = document.toObject(eventModel::class.java)!!


                    var invitationModel = eventInvitationModel()
                    invitationModel.email = tv_email.text.toString()
                    invitationModel.eventId = eventId
                    invitationModel.name = event.typeEvent
                    db.collection("invitationEvent")
                        .document().set(invitationModel)
                        .addOnSuccessListener {
                            sendMail(tv_email.text.toString(),pwd);
                            Toast.makeText(baseContext, getString(R.string.inviteSended),
                                Toast.LENGTH_SHORT).show()
                        }
                }
            }
            .addOnFailureListener { exception ->
                logError(exception.message.toString())
            }
    }

    fun getRandomString(length: Int) : String {
        val charset = "ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz0123456789"
        return (1..length)
            .map { charset.random() }
            .joinToString("")
    }

    fun sendMail(email: String, pwd: String){
        logDebug("SendMail")
        var uri = Uri.parse("mailto:"+email+"?subject="+R.string.inviteSubject.toString()+"&body=Sei stato invitato ad un evento accedi con email:" + email + " pwd: " + pwd)
        startActivity(Intent(Intent.ACTION_VIEW, uri))
        logDebug("ActivityStarted")
    }
}