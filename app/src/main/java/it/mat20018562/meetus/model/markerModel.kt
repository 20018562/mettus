package it.mat20018562.meetus.model

import com.google.android.gms.maps.model.LatLng
import java.io.Serializable

data class markerModel(
    var latitude: Double,
    var longitude: Double,
    var title: String
):Serializable
