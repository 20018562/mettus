package it.mat20018562.meetus.model

import java.time.LocalDate
import java.util.*

data class eventModel(
    var typeEvent: String? = null,
    var date: String? = null,
    var addressEvent: String? = null,
    var latitude: Double? = null,
    var longitude: Double? = null,
    var budget: Double? = null,
    var userIdCreator: String? = null
)

data class eventViewModel(
    var id: String? = null,
    var typeEvent: String? = null,
    var date: String? = null,
    var addressEvent: String? = null,
    var latitude: Double? = null,
    var longitude: Double? = null,
    var budget: Double? = null,
    var userIdCreator: String? = null,

    //var distance: Double? = 0.0
    //val invitationEvent: List<eventInvitationModel>? = null
)

data class eventServiceModel(
    var eventId: String? = null,
    var serviceType: String? = null
)

data class eventInvitationModel(
    var name: String? = null,
    var email: String? = null,
    var eventId: String? = null
)