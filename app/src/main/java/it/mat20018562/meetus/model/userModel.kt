package it.mat20018562.meetus.model

import java.io.Serializable

data class userModel (
    var name: String? = null,
    var surname: String? = null,
    var nameCompany: String?    = null, //fornitore
    var addressCompany: String? = null, //fornitore
    //var coordinates: LatLng? = null,  //fornitore
    var latitude: Double? = null,       //fornitore
    var longitude: Double? = null,      //fornitore

    var piva: String? = null,           //fornitore
    var iban: String? = null,           //fornitore

    var categoryService: String? = null, //use for filter all supplier with categoryService

    var cf: String? = null,             //organizzatore
    var residence: String? = null,      //organizzatore

    var email: String? = null,
    var role: String? = null,

    var editProfile: Int = 0,           //invitato
    var categoryUser: String? = null,   //invitato

    var notifyFeed: String? = null      //tutti
): Serializable

data class userViewModel (
    var id: String? = null,
    var name: String? = null,
    var surname: String? = null,
    var nameCompany: String?    = null, //fornitore
    var addressCompany: String? = null, //fornitore
    //var coordinates: LatLng? = null,  //fornitore
    var latitude: Double? = null,       //fornitore
    var longitude: Double? = null,      //fornitore

    var piva: String? = null,           //fornitore
    var iban: String? = null,           //fornitore

    var categoryService: String? = null, //use for filter all supplier with categoryService

    var cf: String? = null,             //organizzatore
    var residence: String? = null,      //organizzatore

    var email: String? = null,
    var role: String? = null,

    var editProfile: Int = 0,           //invitato
    var categoryUser: String? = null,    //invitato

    var notifyFeed: String? = null,      //tutti

    var distance: Double? = null        //Distance used for filter from current user.
): Serializable
