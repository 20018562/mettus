package it.mat20018562.meetus.model

data class servicePhotoModel(
    var id: String? = null,
    var serviceId: String? = null,
    var type: String? = null
    //var base64: String? = null
)
