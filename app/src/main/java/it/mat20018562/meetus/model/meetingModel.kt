package it.mat20018562.meetus.model

import com.google.android.gms.common.server.converter.StringToIntConverter
import com.google.firebase.Timestamp
import java.time.chrono.ChronoLocalDateTime
import java.time.temporal.TemporalAmount

data class meetingModel (
    var date: String? = null,
    var time: String? = null,
    var request: String? = null,
    var approved: Int? = null,

    var organizerUserId: String? = null,
    var organizerNameSurname: String? = null,

    //var ServiceRequestId: String? = null,

    var EventId: String? = null,
    var supplierUserId: String? = null,

    var quote: Double? = null,
    var acceptedQuote: Int ?= null,

    var paymentMode: String ?= null,
    var paymentMethod: String ?= null,
)

data class meetingViewModel (
    var id: String? = null,
    var dateTime: String? = null,
    var request: String? = null,
    var approved: Int? = null,

    var organizerUserId: String? = null,
    var organizerNameSurname: String? = null,

    //var ServiceRequestId: String? = null,

    var EventId: String? = null,
    var supplierUserId: String? = null,

    var quote: Double? = null,
    var acceptedQuote: Int ?= null,

    var paymentMode: String ?= null,
    var paymentMethod: String ?= null,
)

data class paymentModel(
    var meetingId: String? = null,
    var date: String? = null,
    var confirmed: Int? = null,
)

data class paymentViewModel(
    var id: String? = null,
    var meetingId: String? = null,
    var date: String? = null,
    var confirmed: Int? = null
)