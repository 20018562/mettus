package it.mat20018562.meetus.model

data class serviceModel(
    var userId: String? = null,
    var name: String? = null,
    var companyName: String? = null,
    var description: String? = null,
    var category: String? = null,
    //var timetables: String? = null,
    var timeStart: String = "09:00",
    var timeStop: String = "18:00",
    var price: Double? = null
)

data class serviceViewModel(
    var id: String? = null,
    var userId: String? = null,
    var name: String? = null,
    var companyName: String? = null,
    var description: String? = null,
    var category: String? = null,
    //var timetables: String? = null,
    var timeStart: String = "09:00",
    var timeStop: String = "18:00",
    var price: Double? = null
)

data class serviceEventViewModel(
    var id: String? = null,
    var eventId: String? = null,
    var serviceType: String? = null
)