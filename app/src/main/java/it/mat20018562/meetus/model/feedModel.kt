package it.mat20018562.meetus.model

data class feedModel (
    var text: String? = null,
    var idReplyFeed: String? = null,
    var eventId: String? = null,
    var emailIdCreator: String? = null,
    var nreply: Int = 0
)

data class feedViewModel (
    var id: String? = null,
    var text: String? = null,
    var idReplyFeed: String? = null,
    var eventId: String? = null,
    var emailIdCreator: String? = null,
    var nreply: Int = 0
)
